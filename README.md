# Proef Proeve van Bekwaamheid

### Beschrijving ###

* Dit is het project voor de proef proeve van bekwaamheid. De boardgames waar we de game mecanics van gebruikt hebben.
* **Versie**: 0.1.1.0

### Links & URLS ###

* **Google drive folder:** [Google Drive, Proef Proeve](https://drive.google.com/drive/folders/0B22VQ6sxaPlCc04wRGEtM3ZiRFE)
* **Code Conventions:** [Google Doc, Proef Proeve Code Conventions](https://docs.google.com/document/d/1NcAHGw96FM2x0Ee1XH2pw0EbfwzO3vYaBu66wBYKCh0) *of* [Bitbucket, Conventions.cs](https://bitbucket.org/DaanRuiter/proef-proeve/src/21ca2e0b70f72b8ded6adef0811ef47800f48815/Proef%20Proeve%20Unityproject/Assets/Script/Conventions.cs)
* **Technisch ontwerp:** [Google Doc, Proef Proeve Technisch design](https://docs.google.com/document/d/1DmBIUjATgqzZ5WuC6PIWUJ_Ss7de0_hSQQk2kFESIJQ)
* **IDE document:** [Google Doc, IDE document](https://docs.google.com/document/d/1NP9t3CuCmWRNO1NZqQPTikENx3sWirWXdqV1KWtAeoU)
* **Class Diagram:** [Google Drive, Proef Proeve Class Diagram](https://drive.google.com/open?id=0B22VQ6sxaPlCazRHemttQ1IzbVk)

### Software & IDE ###

* **Game Engine:** Unity Engine 5.5.2f1
* **Code Editor:** Visual Studio
* **Versioning:** GIT, Bitbucket, SourceTree
* **2D Art:** Photoshop, Flash
* **2D Animatie:** Spine, Flash, DOTween

### Code links ###

* [Stop hier links naar classes die boeiend zijn]

### Team ###
#### Art ####
* Cerys Hancock, **Lead**
* Bart Willemsen
* Gerben van de Bosch
* Lotte Hageman
* Luuc Veenker

#### Programming ####
* Menno Jongejan, **Lead**
* Koen van der Velden
* Daan Ruiter