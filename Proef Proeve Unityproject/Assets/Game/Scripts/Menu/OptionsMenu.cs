﻿using UnityEngine;
using UnityEngine.UI;
using ProefProeve.Audio;

public class OptionsMenu : MonoBehaviour {
    [SerializeField] private Slider _musicSlider;
    [SerializeField] private Slider _SFXSlider;


	void Awake ()
    {
        _musicSlider.onValueChanged.AddListener(delegate { OnMusicVolumeSliderChanged(); });
        _SFXSlider.onValueChanged.AddListener(delegate { OnEffectsVolumeSliderChanged(); });
    }

    private void OnMusicVolumeSliderChanged()
    {
        AudioManager.Instance.SetVolume(AudioManager.AudioType.Music, _musicSlider.value);
    }

    private void OnEffectsVolumeSliderChanged()
    {
        AudioManager.Instance.SetVolume(AudioManager.AudioType.SFX, _SFXSlider.value);
    }
}
