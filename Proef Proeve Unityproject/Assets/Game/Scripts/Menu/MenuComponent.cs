﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace ProefProeve.Menu
{
	public class MenuComponent : MonoBehaviour 
	{
		[SerializeField] private CanvasGroup _canvasGroup;
		[SerializeField] private float _fadeDuration;

		/// <summary>
		/// Hides the components.
		/// </summary>
		public void HideComponents()
		{
			transform.DOScale(new Vector3(0.01f,0.01f,0.01f),_fadeDuration);
			DOTween.To (() => _canvasGroup.alpha, x => _canvasGroup.alpha = x, 0, _fadeDuration).SetEase(Ease.OutQuad);
			_canvasGroup.interactable = false;
		}
	
		/// <summary>
		/// Shows the components.
		/// </summary>
		public void ShowComponents()
		{
			transform.DOScale(new Vector3(1,1,1),_fadeDuration);
			DOTween.To (() => _canvasGroup.alpha, x => _canvasGroup.alpha = x, 1, _fadeDuration).SetEase(Ease.InQuad);
			_canvasGroup.interactable = true;
		}
	}
}