﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProefProeve.Menu
{
	public class EnterCharacterSelectionScreen : MonoBehaviour 
	{
		[SerializeField] private List<GameObject> _characters;
        [SerializeField] private List<GameObject> _players;

		/// <summary>
		/// Prepares the char select.
		/// </summary>
		/// <param name="inGamePlayerAmount">In game player amount.</param>
		public void PrepareCharSelect(int inGamePlayerAmount)
		{
			ResetCharSelect ();
			for (int i = 0; i < inGamePlayerAmount; i++) 
			{
                _characters[i].SetActive (true);
			}

            for (int j = 0; j < _players.Count; j++)
            {
                _players[j].SetActive(true);
            }
		}
		public void ResetCharSelect()
		{
			for (int i = 0; i < _characters.Count; i++) 
			{
                _characters[i].SetActive (false);
			}

            for (int j = 0; j < _players.Count; j++)
            {
                _players[j].SetActive(false);
            }
        }
	}
}