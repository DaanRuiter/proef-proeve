﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using ProefProeve.Audio;

namespace ProefProeve.Menu
{
	public class MenuManager : MonoBehaviour 
	{

		[SerializeField] private MenuComponent _mainMenu;
		[SerializeField] private float _sceneLoadDelay;
		[SerializeField] private MenuComponent _charSelect;
		[SerializeField] private MenuComponent _charPicking;
		[SerializeField] private MenuComponent _selectMode;
		[SerializeField] private List<SelectChar> _selectCharacters;
		[SerializeField] private AudioAsset _buttonClick;
		[SerializeField] private AudioAsset backgroundMusic;
		private int _players;
		private int _playersReady;

		public void buttonClicked()
		{
			AudioManager.Instance.Play (_buttonClick, false, true);
		}

		/// <summary>
		/// Loads the next scene.
		/// </summary>
		/// <param name="scene">Scene.</param>
		public void LoadNextScene(string scene)
		{
			StartCoroutine (startNextScene(scene));
		}

		/// <summary>
		/// Starts the next scene.
		/// </summary>
		/// <returns>The next scene.</returns>
		/// <param name="scene">Scene.</param>
		public IEnumerator startNextScene(string scene)
		{
            for (int i = 0; i < _selectCharacters.Count; i++)
            {
                _selectCharacters[i].transform.SetParent(null);
                DontDestroyOnLoad(_selectCharacters[i].gameObject);
            }

            AudioManager.Instance.FadeOut(backgroundMusic);

            ScreenTransition.Instance.FadeTo(1f, _sceneLoadDelay, delegate()
            {
                ScreenTransition.Instance.FadeTo(0f, _sceneLoadDelay);
            });
			yield return new WaitForSeconds (_sceneLoadDelay);
			SceneManager.LoadScene (scene);
		}

		/// <summary>
		/// Quits the game.
		/// </summary>
		public void QuitGame()
		{
			Application.Quit ();
		}

		/// <summary>
		/// Sets the player amount.
		/// </summary>
		/// <param name="amount">Amount.</param>
		public void SetPlayerAmount(int amount)
		{
			_players = amount;
		}

		/// <summary>
		/// Players that are ready.
		/// </summary>
		/// <param name="change">Change.</param>
		public void PlayersReady(int change)
		{
			_playersReady += change;

			if(_playersReady == _players)
			{
				_charSelect.HideComponents ();
				_charPicking.HideComponents ();
				LoadNextScene ("Game");
			}
			else if(_playersReady <= 0)
			{
				_players = 0;
				_playersReady = 0;
			}
				
		}

		/// <summary>
		/// Goes back to mode selection.
		/// </summary>
		public void GoBackToModeSelection()
		{
			foreach (SelectChar character in _selectCharacters) 
			{
				character.ResetCharPickingPositions ();
				character.enabled = false;
			}
			_charPicking.HideComponents ();
			_charSelect.HideComponents ();
			_selectMode.ShowComponents ();
			buttonClicked ();
		}

		void Awake()
		{
			_mainMenu.ShowComponents();
		}

		void Start()
		{
			_players = 0;
			_playersReady = 0;

			AudioManager.Instance.Play (backgroundMusic, true, false);
		}
	}
}
