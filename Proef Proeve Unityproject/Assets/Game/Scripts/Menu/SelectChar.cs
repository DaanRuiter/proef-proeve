﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProefProeve.player;
using UnityEngine.UI;
using ProefProeve.Audio;
using Spine.Unity;
using ProefProeve.Animation;
using UnityEngine.SceneManagement;

namespace ProefProeve.Menu
{
	public class SelectChar : MonoBehaviour 
	{
		private Player _player;
		private int _currentPosition;
		private int _row = 0;
		private int _currentTeam;
		private bool _onCooldown;
		private bool _charSelected;
		private bool _charPicked;

		[SerializeField] private AudioAsset[] _audioClips;
		[SerializeField] private float _cooldown;
		[SerializeField] private SkeletonDataAsset[] _characterSkeletons;
		[SerializeField] private Image _playerImage;
        [SerializeField] private AnimationController _playerAnimationController;
		[SerializeField] private GameObject _hover;
		[SerializeField] private List<GameObject> _characters;
		[SerializeField] private MenuManager _menuManager;
		[SerializeField] private List<Color> _teams;
		[SerializeField] private Image _teamBorder;

		/// <summary>
		/// Resets the char picking positions.
		/// </summary>
		public void ResetCharPickingPositions()
		{
			if (gameObject.activeSelf) 
			{
				_currentPosition = 0;
				_row = 0;
				SetNewPosition ();
				_teamBorder.color = _teams [_player.PlayerID];
				_currentTeam = _player.PlayerID;
				_hover.SetActive (false);

				_charSelected = false;
				_playerImage.color = Color.white;
				_menuManager.PlayersReady (-1);
			}
		}

		void Start () 
		{
			_player = GetComponent<Player>();
			_teamBorder.color = _teams [_player.PlayerID];
			_currentTeam = _player.PlayerID;
            AttachInputListeners();

            _playerAnimationController.gameObject.SetActive(true);
        }

		void Update()
		{

			if (Input.GetKeyDown (KeyCode.A)) 
			{
				OnHorizontalLeftPressed (0);
			}
			if (Input.GetKeyDown (KeyCode.D)) 
			{
				OnHorizontalRightPressed (0);
			}
			if (Input.GetKeyDown (KeyCode.Q)) 
			{
				OnBottomButtonPressed (0);
			}
			if (Input.GetKeyDown (KeyCode.E)) 
			{
				OnRightButtonPressed (0);
			}
			if (Input.GetKeyDown (KeyCode.Z)) 
			{
				OnLeftBumperPressed (0);
			}
			if (Input.GetKeyDown (KeyCode.X)) 
			{
				OnRightBumperPressed (0);
			}
			if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				OnHorizontalLeftPressed (1);
			}
			if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				OnHorizontalRightPressed (1);
			}
			if (Input.GetKeyDown (KeyCode.K)) 
			{
				OnBottomButtonPressed (1);
			}
			if (Input.GetKeyDown (KeyCode.L)) 
			{
				OnRightButtonPressed (1);
			}
			if (Input.GetKeyDown (KeyCode.N)) 
			{
				OnLeftBumperPressed (1);
			}
			if (Input.GetKeyDown (KeyCode.M)) 
			{
				OnRightBumperPressed (1);
			}
			if (Input.GetKeyDown (KeyCode.Space)) 
			{
				OnBottomButtonPressed (2);
				OnBottomButtonPressed (3);
			}
		}
			
		private void OnDestroy()
		{
			DetachInputListeners();
		}

		/// <summary>
		/// Goes on cooldown.
		/// </summary>
		/// <returns>The on cooldown.</returns>
		private IEnumerator GoOnCooldown()
		{
			_onCooldown = true;
			yield return new WaitForSeconds (_cooldown);
			_onCooldown = false;
		}

		private void SetNewPosition()
		{
			StartCoroutine ("GoOnCooldown");
			_hover.transform.position = _characters [_currentPosition].transform.position;
			_hover.transform.SetParent (_characters [_currentPosition].transform);
            _playerAnimationController.SkeletonAnimation.skeletonDataAsset = _characterSkeletons[_currentPosition];
            _playerAnimationController.SkeletonAnimation.skeletonDataAsset.Clear();
            _playerAnimationController.SkeletonAnimation.Initialize(true);
            _playerAnimationController.Play(CharacterAnimations.BASIC_ATTACK);

            playAudio (2);
		}

		private void playAudio(int clipId)
		{
			AudioManager.Instance.Play (_audioClips [clipId], false, true);
		}

		/// <summary>
		/// Raises the bottom button pressed event.
		/// </summary>
		/// <param name="deviceID">Device I.</param>
		private void OnBottomButtonPressed(int deviceID)
		{
			if (deviceID != _player.ControllerID || _onCooldown || _charSelected)
				return;
			
			playAudio (0);

			_charSelected = true;
			_playerImage.color = Color.gray;
            _player.Init(_player.PlayerID, _player.ControllerID, _currentPosition, _currentTeam);
			_menuManager.PlayersReady (1);

            _playerAnimationController.Play(CharacterAnimations.SPECIAL_ATTACK);
        }

		/// <summary>
		/// Raises the bottom button pressed event.
		/// </summary>
		/// <param name="deviceID">Device I.</param>
		private void OnRightButtonPressed(int deviceID)
		{
			if (deviceID != _player.ControllerID || _onCooldown)
				return;

			if(_charSelected)
			{
				playAudio (1);
				_charSelected = false;
				_playerImage.color = Color.white;
				_menuManager.PlayersReady (-1);
			}
			else
			{
				_menuManager.GoBackToModeSelection ();
			}
				
		}

		/// <summary>
		/// Raises the horizontal left pressed event.
		/// </summary>
		/// <param name="deviceID">Device I.</param>
		private void OnHorizontalLeftPressed(int deviceID)
		{
			if (deviceID != _player.ControllerID || _onCooldown || _charSelected)
				return;

			_currentPosition --;

			if (_currentPosition < 0 && _row == 0) 
			{
				_currentPosition = 1;
			} 
			else if(_currentPosition < 2 && _row == 1)
			{
				_currentPosition = 3;
			}
			SetNewPosition ();
		}

		/// <summary>
		/// Raises the horizontal right pressed event.
		/// </summary>
		/// <param name="deviceID">Device I.</param>
		private void OnHorizontalRightPressed(int deviceID)
		{
			if (deviceID != _player.ControllerID || _onCooldown || _charSelected)
				return;

			_currentPosition++;
			if (_currentPosition > 1 && _row == 0) 
			{
				_currentPosition = 0;
			} 
			else if(_currentPosition > 3 && _row == 1)
			{
				_currentPosition = 2;
			}
			SetNewPosition ();
		}

		/// <summary>
		/// Raises the left bumper pressed event.
		/// </summary>
		/// <param name="deviceID">Device I.</param>
		private void OnLeftBumperPressed(int deviceID)
		{
			if (deviceID != _player.ControllerID)
				return;

			_currentTeam--;

			if(_currentTeam < 0)
			{
				_currentTeam = _teams.Count - 1;
			}

			_teamBorder.color = _teams [_currentTeam];
		}

		/// <summary>
		/// Raises the right bumper pressed event.
		/// </summary>
		/// <param name="deviceID">Device I.</param>
		private void OnRightBumperPressed(int deviceID)
		{
			if (deviceID != _player.ControllerID)
				return;

			_currentTeam++;

			if(_currentTeam >= _teams.Count)
			{
				_currentTeam = 0;
			}

			_teamBorder.color = _teams [_currentTeam];
		}


		/// <summary>
		/// Attachs the input listeners.
		/// </summary>
		private void AttachInputListeners()
		{
			InputHandler.Instance.OnBottomButtonDownEvent += OnBottomButtonPressed;
			InputHandler.Instance.OnRightButtonDownEvent += OnRightButtonPressed;

			InputHandler.Instance.OnHorizontalLeftDownEvent += OnHorizontalLeftPressed;
			InputHandler.Instance.OnHorizontalRightDownEvent += OnHorizontalRightPressed;

			InputHandler.Instance.OnAltHorizontalLeftDownEvent += OnHorizontalLeftPressed;
			InputHandler.Instance.OnAltHorizontalRightDownEvent += OnHorizontalRightPressed;

			InputHandler.Instance.OnLeftBumperDownEvent += OnLeftBumperPressed;
			InputHandler.Instance.OnRightBumperDownEvent += OnRightBumperPressed;

            SceneManager.sceneUnloaded += OnLevelUnloaded;
        }

		/// <summary>
		/// Detachs the input listeners.
		/// </summary>
		private void DetachInputListeners()
		{
			InputHandler.Instance.OnBottomButtonDownEvent -= OnBottomButtonPressed;
			InputHandler.Instance.OnHorizontalLeftDownEvent -= OnHorizontalLeftPressed;
			InputHandler.Instance.OnHorizontalRightDownEvent -= OnHorizontalRightPressed;

			InputHandler.Instance.OnAltHorizontalLeftDownEvent -= OnHorizontalLeftPressed;
			InputHandler.Instance.OnAltHorizontalRightDownEvent -= OnHorizontalRightPressed;

            SceneManager.sceneUnloaded -= OnLevelUnloaded;
        }

        private void OnLevelUnloaded(Scene scene)
        {
            //Destroy(_playerAnimationController.gameObject);
        }
	}
}