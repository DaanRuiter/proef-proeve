﻿using ProefProeve.player;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace ProefProeve
{
    /// <summary>
    /// Script that displays stats for players after a match
    /// </summary>
	public class GameOverScreen : MonoBehaviour
	{
        private const string KEY_SETS = "sets";
        private const string KEY_DAMAGE = "damage";
        private const string KEY_POWER_UPS = "powerups";

        [SerializeField] private string winnerText;
        [SerializeField] private string loserText;
        [SerializeField] private Text[] _resultTexts;
        [SerializeField] private Transform[] _statRoots;

        /// <summary>
        /// Initialize game over screen with given players
        /// </summary>
        /// <param name="players">Players to display stats of</param>
        public void Init(Player[] players)
        {
            for (int i = 0; i < players.Length; i++)
            {
                _resultTexts[i].text = players[i].IsAlive ? winnerText : loserText;
                _resultTexts[i].color = MatchInfo.TeamColors[players[i].TeamID];
                _statRoots[i].FindChild(KEY_SETS).GetComponent<Text>().text += string.Format(" <b>{0}</b>", players[i].Stats.SetsCompleted);
                _statRoots[i].FindChild(KEY_DAMAGE).GetComponent<Text>().text += string.Format(" <b>{0}</b>", players[i].Stats.DamageDone);
                _statRoots[i].FindChild(KEY_POWER_UPS).GetComponent<Text>().text += string.Format(" <b>{0}</b>", players[i].Stats.PowerupsUsed);
            }
        }

        /// <summary>
        /// Used for Unity UI button
        /// </summary>
        public void GoToMainMenu()
        {
            ScreenTransition.Instance.FadeTo(1f, 1f, delegate ()
            {
                SceneManager.LoadScene("MainMenu");
                ScreenTransition.Instance.FadeTo(0f, 1f);
            });
        }
	}
}
