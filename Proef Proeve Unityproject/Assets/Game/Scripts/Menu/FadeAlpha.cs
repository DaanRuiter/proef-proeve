﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

namespace ProefProeve.Menu
{
	public class FadeAlpha : MonoBehaviour 
	{
	
		[SerializeField] private Image _image;
		[SerializeField] private float _waitTime;
		[SerializeField] private float _fadeDuration;
		[SerializeField] private string _SceneToLoad;

		void Start () 
		{
			Sequence mySequence = DOTween.Sequence ();
			mySequence.Append (DOTween.ToAlpha (() => _image.color, x => _image.color = x, 1, _fadeDuration))
			.AppendInterval (_waitTime)
			.Append (DOTween.ToAlpha (() => _image.color, x => _image.color = x, 0, _fadeDuration))
			.AppendCallback (() => 
			{
						SceneManager.LoadScene(_SceneToLoad);
			});
		}
	}
}