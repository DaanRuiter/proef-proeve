﻿using ProefProeve.Animation;
using ProefProeve.Grid;
using ProefProeve.player;
using ProefProeve.UI;
using ProefProeve.Audio;
using UnityEngine;
using System;

namespace ProefProeve
{
    public class GameInitializer : MonoBehaviour
    {
        private const string CHARACTER_RESOURCES_PATH = "Characters/";
        private const float CHARACTER_X_OFFSET = 220;

        private readonly string[] CHARACTERS = new string[] { "Lard McKenzie", "Private Brian"};

        public AudioAsset BackgroundMusic
        {
            get
            {
                return _backgroundMusic;
            }
        }

        [SerializeField] private CardSlotGrid _cardGrid;
        [SerializeField] private CardSlotGridPlayerIconGrid[] _playerIconGrids;
        [SerializeField] private Vector3[] _playerSpawnPositions;
		[SerializeField] private AudioAsset _backgroundMusic;

        private MatchInfo _matchInfo;

        private void Initialize()
        {
            _matchInfo = FindObjectOfType<MatchInfo>();

            if (_matchInfo == null)
            {
                Debug.LogError("No matchinfo found! aborting..");
                return;
            }

            // Initializing players.
            Player[] allPlayers = FindObjectsOfType<Player>();
            Array.Sort(allPlayers, (x, y) => string.Compare("" + x.PlayerID, "" + y.PlayerID));

            for (int i = 0; i < allPlayers.Length; i++)
            {
                GameObject character;
                if (CHARACTERS.Length > allPlayers[i].CharacterID)
                {
                    character = SpawnCharacter(allPlayers[i].gameObject, CHARACTERS[allPlayers[i].CharacterID]);
                }
                else //FALLBACK
                {
                    character = SpawnCharacter(allPlayers[i].gameObject, CHARACTERS[0]);
                }

                allPlayers[i].transform.SetParent(transform);
                allPlayers[i].transform.position = _playerSpawnPositions[i];
                allPlayers[i].InitComponents(_cardGrid);

                character.transform.localPosition = new Vector3(-CHARACTER_X_OFFSET, 0, 0);

                if (i % 2 != 0)
                {
                    allPlayers[i].transform.GetComponentInChildren<AnimationController>().transform.localScale = new Vector3(-1, 1, 1);
                    character.transform.localPosition = new Vector3(CHARACTER_X_OFFSET, 0, 0);
                }
            }

            // Setting match info.
            _matchInfo.SetMatchInfo(allPlayers);
            gameObject.RequireComponent<GameStateTracker>().Init(_matchInfo);

            EffectsHandler effectsHandler = GetComponentInChildren<EffectsHandler>();

            if (effectsHandler != null)
                effectsHandler.Initialize(allPlayers);

            SpawnUI(allPlayers);

            _matchInfo.StartGame();
        }

        private void Awake()
        {
            Initialize();
        }

		private void Start()
		{
			AudioManager.Instance.Play (_backgroundMusic, true, false);
		}

        private void SpawnUI(Player[] players)
        {
            for (int i = 0; i < _playerIconGrids.Length; i++)
            {
                _playerIconGrids[i].Init(players);
            }

            for (int i = 0; i < players.Length; i++)
            {
                Player current = players[i];
                current.gameObject.RequireComponent<PlayerUISpawner>();
                current.gameObject.GetComponentInChildren<Camera>().targetTexture = Resources.Load("PlayerPortrait-" + i) as RenderTexture;
            }
        }

        private GameObject SpawnCharacter(GameObject playerRoot, string characterName)
        {
            GameObject prefab = Resources.Load<GameObject>(CHARACTER_RESOURCES_PATH + characterName);
            GameObject instance = Instantiate(prefab);
            instance.transform.SetParent(playerRoot.transform);
            instance.transform.localPosition = Vector3.zero;

            return instance;
        }
	}
}
