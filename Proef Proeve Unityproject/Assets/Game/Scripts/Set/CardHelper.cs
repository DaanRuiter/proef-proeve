﻿using System.Collections.Generic;
using UnityEngine;
using ProefProeve.Powerup;

namespace ProefProeve.Set
{
    public static class CardHelper
    {
        private const string POWERUP_LIST_PATH = "PowerupList";

        private static CardGraphicsLibrary _cardGraphicsLibrary;
        private static PowerupList _powerupList;

        /// <summary>
        /// Validates all cards in given array to see if there are still sets available.
        /// </summary>
        /// <param name="cards"></param>
        /// <returns>true if set still exists in given array; false if there is no set in the given array.</returns>
        public static bool ValidateCards(Card[] cards)
        {
            // First iteration.
            int length = cards.Length;
            for (int i = 0; i < length; i++)
            {
                // Getting card.
                Card firstCardToCheck = cards[i];
                // Second iteration.
                for (int j = 0; j < length; j++)
                {
                    // Getting second card to check.
                    Card secondCardToCheck = cards[j];
                    if (secondCardToCheck != firstCardToCheck)
                    {
                        for (int l = 0; l < length; l++)
                        {
                            // Getting third card to check.
                            Card lastCardToCheck = cards[l];
                            if (lastCardToCheck != firstCardToCheck && lastCardToCheck != secondCardToCheck && CheckSet(firstCardToCheck, secondCardToCheck, lastCardToCheck))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            
            return false;
        }

        /// <summary>
        /// Checks one set of three cards if the rules are valid.
        /// </summary>
        /// <returns>True if set has the correct set rules, otherwise returns false.</returns>
        public static bool CheckSet(Card cardOne, Card cardTwo, Card cardThree)
        {
            // Doing three checks to see if the rules apply to all the three cards.
            bool firstColorCheck = CheckRule((int)cardOne.Properties.CardColor, (int)cardTwo.Properties.CardColor);
            bool firstElementCheck = CheckRule((int)cardOne.Properties.CardElement, (int)cardTwo.Properties.CardElement);

            bool secondColorCheck = CheckRule((int)cardTwo.Properties.CardColor, (int)cardThree.Properties.CardColor);
            bool secondElementCheck = CheckRule((int)cardTwo.Properties.CardElement, (int)cardThree.Properties.CardElement);

            bool thirdColorCheck = CheckRule((int)cardThree.Properties.CardColor, (int)cardOne.Properties.CardColor);
            bool thirdElementCheck = CheckRule((int)cardThree.Properties.CardElement, (int)cardOne.Properties.CardElement);

            bool fullColorCheck = (firstColorCheck == secondColorCheck && secondColorCheck == thirdColorCheck);
            bool fullElementCheck = (firstElementCheck == secondElementCheck && secondElementCheck == thirdElementCheck);

            // Returning true if the rules are all correct (All true or all false.)
            return (fullColorCheck && fullElementCheck);
        }

        /// <summary>
        /// Checks one set of three cards if the rules are valid.
        /// </summary>
        /// <returns>True if set has the correct set rules, otherwise returns false.</returns>
        public static bool CheckSet(Card[] cards)
        {
            if (cards.Length > 3)
            {
                Debug.LogWarning("[CardHelper] Card array length bigger then 3 returning false!");
                return false;
            }
            else if(cards.Length < 3)
            {
                Debug.LogWarning("[CardHelper] Card array length smaller then 3 returning false!");
                return false;
            }

            return CheckSet(cards[0], cards[1], cards[2]);
        }

        /// <summary>
        /// Creates a card game object with the given properties.
        /// </summary>
        public static GameObject CreateCard(CardProperties properties)
        {
            if (_cardGraphicsLibrary == null)
            {
                _cardGraphicsLibrary = Resources.Load("CardGraphicsLibrary") as CardGraphicsLibrary;
            }
            
            GameObject cardGameObject = Object.Instantiate(Resources.Load("Card") as GameObject);
            Card card = cardGameObject.GetComponent<Card>();
            card.SetupCard(properties);
            card.ElementSpriteRenderer.sprite = _cardGraphicsLibrary.GetSpriteForElement(properties.CardElement);
            card.BackgroundSpriteRenderer.sprite = _cardGraphicsLibrary.GetBackgroundForColor(properties.CardColor);           
            card.BorderSpriteRenderer.sprite = _cardGraphicsLibrary.GetBorderForGrade(properties.CardGrade);

            return cardGameObject;
        }

        /// <summary>
        /// Creates a pickup and gives it a random powerup then sets it on the given card.
        /// </summary>
        public static void SetRandomPickup(Card card)
        {
            if (_powerupList == null)
            {
                _powerupList = Resources.Load(POWERUP_LIST_PATH, typeof(PowerupList)) as PowerupList;
            }

            UseablePowerup[] powerups = _powerupList.List;
            Sprite[] icons = _powerupList.Icons;
            int length = powerups.Length;
            int random = Random.Range(0, length);

            UseablePowerup randomPowerup = powerups[random];
            card.UseablePowerup = randomPowerup;
            card.SetPowerupIcon(icons[random], _powerupList.IconScale, _powerupList.IconYOffset);
        }

        /// <summary>
        /// Generates a random set to be used at the start of generating a new grid
        /// </summary>
        /// <returns></returns>
        public static CardProperties[] GenerateRandomSetProperties()
        {
            CardProperties[] result = new CardProperties[3];
            bool colorSame = Random.value >= 0.5f ? true : false;
            bool elementSame = Random.value >= 0.5f ? true : false;
            
            CardProperties first = CardProperties.Random;
            CardProperties second = new CardProperties();
            CardProperties third = new CardProperties();

            if (colorSame)
            {
                second.CardColor = first.CardColor;
                third.CardColor = first.CardColor;
            }
            else
            {
                List<CardProperties.Color> colors = new List<CardProperties.Color>();
                colors.AddRange(CardProperties.AllColors);

                colors.Remove(first.CardColor);
                second.CardColor = colors[Random.Range(0, colors.Count)];
                colors.Remove(second.CardColor);
                third.CardColor = colors[Random.Range(0, colors.Count)];
            }

            if (elementSame)
            {
                second.CardElement = CardProperties.Element.Air;
                third.CardElement = CardProperties.Element.Fire;
            }
            else
            {
                List<CardProperties.Element> elements = new List<CardProperties.Element>();
                elements.AddRange(CardProperties.AllElements);

                elements.Remove(first.CardElement);
                second.CardElement = elements[Random.Range(0, elements.Count)];
                elements.Remove(second.CardElement);
                third.CardElement = elements[Random.Range(0, elements.Count)];
            }

            second.RandomizeGrade();
            third.RandomizeGrade();

            result[0] = first;
            result[1] = second;
            result[2] = third;
            return result;
        }

        /// <summary>
        /// Returns the UnityEngine.Color for the correspinding CardProperties.Color.
        /// </summary>
        /// <param name="color">CardProperties.Color to get the Unity color of.</param>
        /// <returns></returns>
        public static Color CastCardColor(CardProperties.Color color)
        {
            switch (color)
            {
                case CardProperties.Color.Red:
                    return Color.red;
                case CardProperties.Color.Green:
                    return Color.green;
                case CardProperties.Color.Blue:
                    return Color.blue;
                case CardProperties.Color.Yellow:
                    return Color.yellow;
                default:
                    return Color.white;
            }
        }

        /// <summary>
        /// Return true if the properties are the same else returns false if not the same.
        /// </summary>
        /// <param name="propertieOne">First card propertie</param>
        /// <param name="propertieTwo">Second card propertie</param>
        /// <returns>Return true if the properties are the same else returns false if not the same.</returns>
        private static bool CheckRule(int propertieOne, int propertieTwo)
        {
            if (propertieOne == propertieTwo)
            {
                return true;
            }

            return false;
        }

    }

}
