﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace ProefProeve.Set
{
    public enum CardTweenState
    {
        Up,
        None,
        Down
    }

    public class Card : MonoBehaviour
    {
        public const int UI_SCALE = 420;
        /// <summary>
        /// Properties of the card, such as element, color and grade.
        /// </summary>
        public CardProperties Properties
        {
            get
            {
                return _properties;
            }
        }

        /// <summary>
        /// Reference of the element sprite renderer
        /// </summary>
        public SpriteRenderer ElementSpriteRenderer
        {
            get
            {
                return _elementSpriteRenderer;
            }
        }

        /// <summary>
        /// Reference of the element sprite renderer
        /// </summary>
        public SpriteRenderer BorderSpriteRenderer
        {
            get
            {
                return _borderSpriteRenderer;
            }
        }

        /// <summary>
        /// Reference of the background sprite renderer
        /// </summary>
        public SpriteRenderer BackgroundSpriteRenderer
        {
            get
            {
                return _backgroundSpriteRenderer;
            }
        }

        /// <summary>
        /// The card's damage
        /// </summary>
        public int Damage
        {
            get
            {
                return (int)_properties.CardGrade;
            }
        }

        public Powerup.UseablePowerup UseablePowerup
        {
            get
            {
                return _properties.Powerup;
            }
            set
            {
                _properties.Powerup = value;
            }
        }

        public CardTweenState TweenState
        {
            get
            {
                return _tweenState;
            }
        }


        public int GridIndex;
        
        [Header("References")]
        [SerializeField] private SpriteRenderer _elementSpriteRenderer;
        [SerializeField] private SpriteRenderer _borderSpriteRenderer;
        [SerializeField] private SpriteRenderer _backgroundSpriteRenderer;
        [SerializeField] private SpriteRenderer _powerupIcon;

        [Header("DOTween settings")]
        [SerializeField] private AnimationCurve _scaleUpAnimationCurve;
        [SerializeField] private AnimationCurve _rotationUpAnimationCurve;
        [SerializeField] private float _tweenUpDuration;
 
        // Variables
        private CardProperties _properties;
        private CardTweenState _tweenState;
        private Tweener _scaleTweener;
        private Tweener _rotationTweener;
        
        // Methods

        /// <summary>
        /// Setup card with element and color. Grade is randomized
        /// </summary>
        /// <param name="element"></param>
        /// <param name="color"></param>
        public void SetupCard(CardProperties.Element element, CardProperties.Color color)
        {
            // Setting properties of card.
            _properties.CardColor = color;
            _properties.CardElement = element;

            // Randomising grade.
            _properties.RandomizeGrade();

            _powerupIcon.gameObject.SetActive(false);
        }

        public void SetupCard(CardProperties properties)
        {
            _properties.CardColor = properties.CardColor;
            _properties.CardElement = properties.CardElement;
            _properties.CardGrade = properties.CardGrade;
            _properties.Powerup = properties.Powerup;

            //_powerupIcon.gameObject.SetActive(false);
        }

        /// <summary>
        /// Is this a card to be displayed in UI or is it a card to be ised in the game
        /// </summary>
        /// <param name="newMode">true = UI, false = game view</param>
        public void SetUIScale(bool newMode)
        {
            if (newMode)
            {
                transform.localScale = Vector3.one * UI_SCALE;
            }
            else
            {
                transform.localScale = Vector3.one;
            }
        }

        /// <summary>
        /// Sets powerup icon.
        /// </summary>
        /// <param name="sprite">Icon of powerup.</param>
        public void SetPowerupIcon(Sprite sprite, float scale, float yOffset)
        {
            _powerupIcon.sprite = sprite;
            _powerupIcon.transform.localScale = new Vector3(scale, scale, 1f);
            _powerupIcon.transform.localPosition = new Vector2(0f, yOffset);
            _powerupIcon.gameObject.SetActive(true);
        }
        
        public IEnumerator StartRemoveCardTweens(float delayPerCardSeconds, float endScale, Vector3 rotationAdd, float duration, int delayMultiplier = 0, TweenCallback onCompleteCallback = null)
        {
            float delaySeconds = delayPerCardSeconds * delayMultiplier;
            _tweenState = CardTweenState.Down;
            yield return new WaitForSeconds(delaySeconds);

            if (this == null) //prevent rare tween + coroutine error
                yield break;

            if (_scaleTweener != null)
                _scaleTweener.Kill();

            if (_rotationTweener != null)
                _rotationTweener.Kill();

            _scaleTweener = transform.DOScale(endScale, duration);
            _rotationTweener = transform.DOLocalRotate(transform.localEulerAngles + rotationAdd, duration);

            SpriteRenderer[] spriteRenderers = transform.GetComponentsInChildren<SpriteRenderer>();
            for (int i = 0; i < spriteRenderers.Length; i++)
            {
                spriteRenderers[i].DOFade(0, duration);
            }

            yield return new WaitForSeconds(duration);

            if (onCompleteCallback != null)
            {
                onCompleteCallback();
            }

            _tweenState = CardTweenState.None;

            if (this == null) //prevent rare tween + coroutine error
                yield break;

            Destroy(gameObject);
        }

        public IEnumerator StartShowCardTweensUI(TweenCallback onCompleteCallback = null)
        {
            transform.localScale = Vector3.zero;
            transform.DOScale(UI_SCALE, _tweenUpDuration);
            transform.DOLocalRotate(transform.localEulerAngles + new Vector3(0, 0, 360), _tweenUpDuration, RotateMode.FastBeyond360);
            _tweenState = CardTweenState.Up;

            yield return new WaitForSeconds(_tweenUpDuration);

            if (onCompleteCallback != null)
            {
                onCompleteCallback();
            }

            _tweenState = CardTweenState.None;
        }

        public IEnumerator StartShowCardTweens(float delayPerCardSeconds, float duration, int delayMultiplier = 0, TweenCallback onCompleteCallback = null)
        {
            float delaySeconds = delayPerCardSeconds * delayMultiplier;

            transform.localScale = Vector3.zero;
            _tweenState = CardTweenState.Up;

            yield return new WaitForSeconds(delaySeconds);

            if (_scaleTweener != null)
                _scaleTweener.Kill();

            if (_rotationTweener != null)
                _rotationTweener.Kill();

            _scaleTweener = transform.DOScale(1f, duration).SetEase(_scaleUpAnimationCurve);
            _rotationTweener = transform.DOLocalRotate(transform.localEulerAngles + new Vector3(0, 0, 360), duration, RotateMode.FastBeyond360).SetEase(_rotationUpAnimationCurve);

            SpriteRenderer[] spriteRenderers = transform.GetComponentsInChildren<SpriteRenderer>();
            for (int i = 0; i < spriteRenderers.Length; i++)
            {
                Color spriteColor = spriteRenderers[i].color;
                spriteColor.a = 0f;
                spriteRenderers[i].color = spriteColor;
                spriteRenderers[i].DOFade(1f, duration);
            }

            yield return new WaitForSeconds(duration);

            if (onCompleteCallback != null)
            {
                onCompleteCallback();
            }

            _tweenState = CardTweenState.None;
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }

        private void ShowPowerupIcon()
        {

        }
    }

}