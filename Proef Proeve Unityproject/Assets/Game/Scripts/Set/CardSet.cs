﻿using UnityEngine;

namespace ProefProeve.Set
{
    /// <summary>
    /// Data model for a set of cards
    /// </summary>
	public class CardSet
	{
        public const int CARDS_REQUIRED_FOR_SET = 3;

        public delegate void CardsChangedEventHandler(int index, Card newCard);
        public event CardsChangedEventHandler CardChangedEvent;
        public int[] CardGridIndexes
        {
            get
            {
                return _cardGridIndexes;
            }
        }

        /// <summary>
        /// Has the current set been validated and is the set correct.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return _hasCurrentSetBeenValidated && _isSetValid;
            }
        }

        public Card[] Cards
        {
            get
            {
                return _cards;
            }
        }
        
        private Card[] _cards;
        private int[] _cardGridIndexes;
        private bool _hasCurrentSetBeenValidated;
        private bool _isSetValid;

        /// <summary>
        /// Creates a new set object with an empty Card array with the size of CARDS_REQUIRED_FOR_SET.
        /// </summary>
        public CardSet()
        {
            _cards = new Card[CARDS_REQUIRED_FOR_SET];
        }

        /// <summary>
        /// Creates a new set object with the given cards. Array length must be CARDS_REQUIRED_FOR_SET.
        /// </summary>
        /// <param name="cards"></param>
        public CardSet(Card[] cards)
        {
            if (cards.Length != CARDS_REQUIRED_FOR_SET)
            {
                Debug.LogError(string.Format("Cards array size not correct, size: {0}, expected: {1}", cards.Length, CARDS_REQUIRED_FOR_SET));
                _cards = new Card[CARDS_REQUIRED_FOR_SET];
            }
            else
            {
                _cards = cards;
            }
        }

        /// <summary>
        /// Clear the cards currently occupying the set.
        /// </summary>
        public void Clear()
        {
            for (int i = 0; i < CARDS_REQUIRED_FOR_SET; i++)
            {
                SetCard(i, null, false);
            }
        }

        /// <summary>
        /// Set a card in the set at a specific index.
        /// </summary>
        /// <param name="index">Where the card shoud be set in the card set.</param>
        /// <param name="card">Card to set at specified index.</param>
        public void SetCard(int index, Card card, bool sendEvent = true)
        {
            if (index < 0 || index > CARDS_REQUIRED_FOR_SET)
            {
                Debug.LogError(string.Format("Card index out of range, index: {0}", index));
                return;
            }

            _cards[index] = card;
            _hasCurrentSetBeenValidated = false;

            if (sendEvent && CardChangedEvent != null)
            {
                CardChangedEvent(index, card);
            }
        }

        /// <summary>
        /// Get the card at a specific index.
        /// </summary>
        /// <param name="index">Index of card to return</param>
        /// <returns>Card at specified index. Returns null if index is out of range of if no card has been set.</returns>
        public Card GetCard(int index)
        {
            if (index < 0 || index >= CARDS_REQUIRED_FOR_SET)
            {
                Debug.LogError(string.Format("Card index out of range, index: {0}", index));
                return null;
            }

            return _cards[index];
        }

        /// <summary>
        /// Checks the set if the current set has not been validated yet.
        /// </summary>
        /// <returns>The result of CardHelper.CheckSet([Cards]).</returns>
        public bool Validate()
        {
            if (_hasCurrentSetBeenValidated)
            {
                return _isSetValid;
            }

            _hasCurrentSetBeenValidated = true;
            return _isSetValid = CardHelper.CheckSet(_cards);
        }

        /// <summary>
        /// Returns the index of the first empty slot in the set.
        /// </summary>
        /// <returns>Index of the first empty slot, returns -1 if the set is full.</returns>
        public int GetFirstEmptyIndex()
        {
            for (int i = 0; i < _cards.Length; i++)
            {
                if (_cards[i] == null)
                    return i;
            }
            return -1;
        }

        public int GetLastFilledSlotIndex()
        {
            for (int i = _cards.Length - 1; i >= 0; i--)
            {
                if (_cards[i] != null)
                    return i;
            }
            return -1;
        }
	}
}