﻿using System;

namespace ProefProeve.Set
{
    public struct CardProperties
    {
        /// <summary>
        /// Chance to get lowest grade card.
        /// </summary>
        public const float LOWEST_CHANCE = 50;
        /// <summary>
        /// Chance to get middle grade card.
        /// </summary>
        public const float MIDDLE_CHANCE = 25;
        /// <summary>
        /// Chance to get highest grade card.
        /// </summary>
        public const float HIGHEST_CHANCE = 10;

        /// <summary>
        /// Chance to drop a powerup on set complete.
        /// </summary>
        public const float POWERUP_CHANCE = 10;

        /// <summary>
        /// All the colors in the enum
        /// </summary>
        public static readonly Color[] AllColors = new Color[4]
        {
            Color.Blue,
            Color.Green,
            Color.Red,
            Color.Yellow
        };

        public static readonly Element[] AllElements = new Element[4]
        {
            Element.Fire,
            Element.Water,
            Element.Earth,
            Element.Air
        };
        
        public enum Element
        {
            Fire = 0,
            Water,
            Earth,
            Air
        }

        /// <summary>
        /// Background color of the card
        /// </summary>
        public enum Color
        {
            Red = 0,
            Green,
            Blue,
            Yellow
        }

        /// <summary>
        /// The grades for the cards with the damage as the flag
        /// </summary>
        public enum Grade
        {
            Low = 5,
            Middle = 10,
            High = 20
        }
        
        public Color CardColor;
        public Element CardElement;
        public Grade CardGrade;

        public Powerup.UseablePowerup Powerup;

        /// <summary>
        /// Generates a random CardProperties object
        /// </summary>
        public static CardProperties Random
        {
            get
            {
                CardProperties result = new CardProperties()
                {
                    CardColor = (Color)UnityEngine.Random.Range(0, 4),
                    CardElement = (Element)UnityEngine.Random.Range(0, 4)
                };

                result.RandomizeGrade();
                return result;
            }
        }
        
        public void RandomizeGrade()
        {
            int chance = UnityEngine.Random.Range(0, 100);
            if (chance <= CardProperties.HIGHEST_CHANCE)
            {
                CardGrade = CardProperties.Grade.High;
            }
            else if (chance <= CardProperties.MIDDLE_CHANCE)
            {
                CardGrade = CardProperties.Grade.Middle;
            }
            else
            {
                CardGrade = CardProperties.Grade.Low;
            }
        }

        public override string ToString()
        {
            return string.Format("Grade={0},Element={1},Color={2}", CardGrade, CardElement, CardColor);
        }
    }

}
