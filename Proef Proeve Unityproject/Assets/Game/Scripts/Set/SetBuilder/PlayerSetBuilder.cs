﻿using System;
using ProefProeve.Grid;
using ProefProeve.player;
using UnityEngine;

namespace ProefProeve.Set
{
    /// <summary>
    /// Allows players to create sets and select groups with controller input
    /// </summary>
	public class PlayerSetBuilder : MonoBehaviour
	{
        /// <summary>
        /// Eventhandler for when the player creates a set, correct or incorrect.
        /// </summary>
        /// <param name="set">Created set</param>
        public delegate void PlayerSetCreatedEventHandler(int playerID, CardSet set);
        /// <summary>
        /// Eventhandler for when a card has been added or removed from the player set.
        /// </summary>
        /// <param name="setCardIndex">Index of the card in the set.</param>
        /// <param name="selectedCard">Newly added/removed card to the set.</param>
        public delegate void PlayerSetContentChangedEventHandler(int playerID, int setCardIndex, Card selectedCard);
        /// <summary>
        /// Eventhandler for when the player switches between CardSlotGroups.
        /// </summary>
        public delegate void CardSlotGroupSwitchEventHandler(int playerID, CardSlotGroup previousGroup, CardSlotGroup newGroup);
        /// <summary>
        /// Event for when the player switches between CardSlotGroups.
        /// </summary>
        public event CardSlotGroupSwitchEventHandler CardSlotGroupSwitchEvent;
        /// <summary>
        /// Event for when a card is added or removed from the player's set.
        /// </summary>
        public event PlayerSetContentChangedEventHandler SetContentChangedEvent;
        /// <summary>
        /// Event for when the player has created a set of atleast CardSet.REQUIRED_CARDS_FOR_SET cards.
        /// </summary>
        public event PlayerSetCreatedEventHandler SetCreatedEvent;

        /// <summary>
        /// End scale for cards being used for the spawn tween
        /// </summary>
        public float CardRemovedEndScale
        {
            get
            {
                return _cardRemovedEndScale;
            }
        }
        /// <summary>
        /// Duration of the tween for when a card is removed
        /// </summary>
        public float CardRemovedTweenDuration
        {
            get
            {
                return _cardRemovedTweenDuration;
            }
        }
        /// <summary>
        /// Euler angles being added to the starting rotation for a card when it's removed
        /// </summary>
        public Vector3 CardRemovedRotationAdd
        {
            get
            {
                return _cardRemovedRotationAdd;
            }
        }
        /// <summary>
        /// Amount of seconds multiplied by the index of the card that's used for the delay of the cards spawn tweens
        /// </summary>
        public float DelayPerCardSeconds
        {
            get
            {
                return _delayPerCardSeconds;
            }
        }

        [Header("References")]
        [Header("Config")]
        [SerializeField] private float _playerSetCreationCooldown;
        [Header("Tween settings")]
        [SerializeField] private float _cardRemovedEndScale;
        [SerializeField] private float _cardRemovedTweenDuration;
        [SerializeField] private Vector3 _cardRemovedRotationAdd;
        [SerializeField] private float _delayPerCardSeconds;

        private CardSlotGrid _grid;
        private Player _player;
        private CardSet _set;
        private CardSlotGroup.CardSlotGroupPosition _currentGroupPosition;
        private float _LastSetCreationTimeStamp;
        private bool _isAllowedToCreateSet;

        /// <summary>
        /// Initialize the set builder with the given grid as target
        /// </summary>
        /// <param name="grid">Target to build cards on</param>
        public void Init(CardSlotGrid grid)
        {
            _grid = grid;
            _player = GetComponent<Player>();
            _set = new CardSet();
            _set.CardChangedEvent += OnSetChanged;
            _currentGroupPosition = CardSlotGroup.CardSlotGroupPosition.Center;
            _LastSetCreationTimeStamp = -1;
            _isAllowedToCreateSet = true;

            Invoke("AttachGameStateListeners", Time.maximumDeltaTime);
        }

        private void Update()
        {
            if (_LastSetCreationTimeStamp > 0 && Time.time >= _LastSetCreationTimeStamp + _playerSetCreationCooldown)
            {
                _LastSetCreationTimeStamp = -1;
                _isAllowedToCreateSet = true;
            }
        }

        private void OnDestroy()
        {
            _set.CardChangedEvent -= OnSetChanged;

            DetachListeners();
            DetachGameStateListeners();
        }

        private void OnSetChanged(int index, Card newCard)
        {
            if (SetContentChangedEvent != null)
            {
                SetContentChangedEvent(_player.PlayerID, index, newCard);
            }
        }

        /// <summary>
        /// Select a card from the grid with the given index
        /// </summary>
        /// <param name="cardGroupIndex">Index of the card in the grid</param>
        private void SelectCard(int cardGroupIndex)
        {
            if (!_isAllowedToCreateSet || _player.IsStunned)
                return;

            int firstEmptyIndex = _set.GetFirstEmptyIndex();

            if (firstEmptyIndex >= 0)
            {
                Card card = _grid.GetSlotGroup(_currentGroupPosition).CardSlots[cardGroupIndex].Card;

                if (card == null)
                    return;

                for (int i = 0; i < _set.Cards.Length; i++)
                {
                    if (_set.Cards[i] != null && _set.Cards[i].GridIndex == card.GridIndex)
                        return;
                }

                GameObject cardView = CardHelper.CreateCard(card.Properties);
                Card cardViewScript = cardView.GetComponent<Card>();
                cardViewScript.GridIndex = card.GridIndex;
                _set.SetCard(firstEmptyIndex, cardView.GetComponent<Card>());


                if (_set.GetCard(CardSet.CARDS_REQUIRED_FOR_SET - 1) != null)
                {
                    _set.Validate();
                    _LastSetCreationTimeStamp = Time.time + _playerSetCreationCooldown;
                    _isAllowedToCreateSet = false;
                    
                    if (SetCreatedEvent != null)
                    {
                        SetCreatedEvent(_player.PlayerID, _set);
                    }

                    for (int i = 0; i < CardSet.CARDS_REQUIRED_FOR_SET; i++)
                    {
                        _grid.RandomizeCardAt(_set.Cards[i].GridIndex);
                    }

                    _set.Clear();
                }
            }
            else
            {
                Debug.LogError("Wrong set index: " + firstEmptyIndex);
            }
        }

        /// <summary>
        /// Remove the last card from the players set (right to left)
        /// </summary>
        private void RemoveLastCardFromSet()
        {
            int lastFilledSlotIndex = _set.GetLastFilledSlotIndex();

            if (lastFilledSlotIndex >= 0)
            {
                _set.SetCard(lastFilledSlotIndex, null);
            }
        }

        private void OnLeftTriggerPressed(int deviceID)
        {
            if (deviceID != _player.ControllerID || !isActiveAndEnabled)
                return;

            if (CardSlotGroupSwitchEvent != null)
            {
                CardSlotGroupSwitchEvent(_player.PlayerID, _grid.GetSlotGroup(_currentGroupPosition), _grid.GetSlotGroup(CardSlotGroup.CardSlotGroupPosition.Left));
            }

            _currentGroupPosition = CardSlotGroup.CardSlotGroupPosition.Left;
        }

        private void OnRightTriggerPressed(int deviceID)
        {
            if (deviceID != _player.ControllerID || !isActiveAndEnabled)
                return;

            if (CardSlotGroupSwitchEvent != null)
            {
                CardSlotGroupSwitchEvent(_player.PlayerID, _grid.GetSlotGroup(_currentGroupPosition), _grid.GetSlotGroup(CardSlotGroup.CardSlotGroupPosition.Right));
            }

            _currentGroupPosition = CardSlotGroup.CardSlotGroupPosition.Right;
        }

        private void OnTriggersReleased(int deviceID)
        {
            if (deviceID != _player.ControllerID || !isActiveAndEnabled)
                return;

            if (CardSlotGroupSwitchEvent != null)
            {
                CardSlotGroupSwitchEvent(_player.PlayerID, _grid.GetSlotGroup(_currentGroupPosition), _grid.GetSlotGroup(CardSlotGroup.CardSlotGroupPosition.Center));
            }

            _currentGroupPosition = CardSlotGroup.CardSlotGroupPosition.Center;
        }

        private void OnLeftBumperPressed(int deviceID)
        {
            if (deviceID != _player.ControllerID)
                return;

            RemoveLastCardFromSet();
        }

        private void OnTopButtonPressed(int deviceID)
        {
            if (deviceID != _player.ControllerID)
                return;

            SelectCard(0);
        }

        private void OnLeftButtonPressed(int deviceID)
        {
            if (deviceID != _player.ControllerID)
                return;


            SelectCard(1);
        }

        private void OnRightButtonPressed(int deviceID)
        {
            if (deviceID != _player.ControllerID)
                return;


            SelectCard(2);
        }
        
        private void OnBottomButtonPressed(int deviceID)
        {
            if (deviceID != _player.ControllerID)
                return;


            SelectCard(3);
        }

        private void OnOtherPlayerSetCreated(int playerID, CardSet otherSet)
        {
            for (int i = 0; i < otherSet.Cards.Length; i++)
            {
                int firstEmptyIndex = _set.GetFirstEmptyIndex();
                for (int j = 0; j < firstEmptyIndex; j++)
                {
                    if (_set.Cards[j].GridIndex == otherSet.Cards[i].GridIndex)
                    {
                        _set.SetCard(j, null);
                    }
                }
            }
        }

        private void AttachGameStateListeners()
        {
            MatchInfo.OnGameStartedEvent += AttachListeners;
            MatchInfo.OnGamePausedEvent += DetachListeners;
            MatchInfo.OnGameResumedEvent += AttachListeners;
        }

        private void DetachGameStateListeners()
        {
            MatchInfo.OnGameStartedEvent -= AttachListeners;
            MatchInfo.OnGamePausedEvent -= DetachListeners;
            MatchInfo.OnGameResumedEvent -= AttachListeners;
        }

        /// <summary>
        /// Add listeners to controller input & other player set builders
        /// </summary>
        private void AttachListeners()
        {
            InputHandler.Instance.OnLeftTriggerDownEvent += OnLeftTriggerPressed;
            InputHandler.Instance.OnRightTriggerDownEvent += OnRightTriggerPressed;
            InputHandler.Instance.OnTriggersReleasedEvent += OnTriggersReleased;
            InputHandler.Instance.OnLeftBumperDownEvent += OnLeftBumperPressed;
            InputHandler.Instance.OnLeftButtonDownEvent += OnLeftButtonPressed;
            InputHandler.Instance.OnTopButtonDownEvent += OnTopButtonPressed;
            InputHandler.Instance.OnRightButtonDownEvent += OnRightButtonPressed;
            InputHandler.Instance.OnBottomButtonDownEvent += OnBottomButtonPressed;

            PlayerSetBuilder[] setBuilders = transform.parent.GetComponentsInChildren<PlayerSetBuilder>();
            for (int i = 0; i < setBuilders.Length; i++)
            {
                if (setBuilders[i] == this)
                    continue;

                setBuilders[i].SetCreatedEvent += OnOtherPlayerSetCreated;
            }
        }

        /// <summary>
        /// remove listeners to controller input & other player set builders
        /// </summary>
        private void DetachListeners()
        {
            InputHandler.Instance.OnLeftTriggerDownEvent -= OnLeftTriggerPressed;
            InputHandler.Instance.OnRightTriggerDownEvent -= OnRightTriggerPressed;
            InputHandler.Instance.OnTriggersReleasedEvent -= OnTriggersReleased;
            InputHandler.Instance.OnLeftBumperDownEvent -= OnLeftBumperPressed;
            InputHandler.Instance.OnLeftButtonDownEvent -= OnLeftButtonPressed;
            InputHandler.Instance.OnTopButtonDownEvent -= OnTopButtonPressed;
            InputHandler.Instance.OnRightButtonDownEvent -= OnRightButtonPressed;
            InputHandler.Instance.OnBottomButtonDownEvent -= OnBottomButtonPressed;

            PlayerSetBuilder[] setBuilders = transform.parent.GetComponentsInChildren<PlayerSetBuilder>();
            for (int i = 0; i < setBuilders.Length; i++)
            {
                if (setBuilders[i] == this)
                    continue;

                setBuilders[i].SetCreatedEvent -= OnOtherPlayerSetCreated;
            }
        }
    }
}