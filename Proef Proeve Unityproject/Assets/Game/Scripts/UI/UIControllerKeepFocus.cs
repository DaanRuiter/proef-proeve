﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ProefProeve.UI
{
	public class UIControllerKeepFocus : MonoBehaviour
	{
        void Update()
        {
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                EventSystem.current.SetSelectedGameObject(EventSystem.current.firstSelectedGameObject);
            }
        }
    }
}
