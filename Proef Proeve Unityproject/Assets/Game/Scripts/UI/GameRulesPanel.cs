﻿using DG.Tweening;
using UnityEngine;
using System.Collections;
using ProefProeve.Audio;
using UnityEngine.UI;

namespace ProefProeve.UI
{
    public class GameRulesPanel : MonoBehaviour
    {
        public delegate void GameRulesPanelEventHandler();
        public event GameRulesPanelEventHandler PageSwitchedEvent;
        public event GameRulesPanelEventHandler PagesReadEvent;

        [SerializeField] private Sprite[] _slides = new Sprite[0];
		[SerializeField] private AudioAsset _switchSlide;
		[SerializeField] private AudioAsset[] _countdownAudio;
		[SerializeField] private GameObject _countdownTextObject;
		[SerializeField] private Text _countdownText;

        private Vector3 _oldPosition;
        private SpriteRenderer _renderer;

        private int _slideCounter = 0;

        private void Awake()
        {
            //Getters
            _renderer = GetComponent<SpriteRenderer>();
            _oldPosition = this.transform.position;
            _renderer.sprite = _slides[0];

            //Movement
            transform.DOMove(Vector3.zero, 1, false);
            InputHandler.Instance.OnBottomButtonDownEvent += OnBottomButtonDownEvent;
        }

        private void OnBottomButtonDownEvent(int deviceID)
        {
            _slideCounter++;

			AudioManager.Instance.Play(_switchSlide,false,true);

            if (_slideCounter >= _slides.Length)
            {
                InputHandler.Instance.OnBottomButtonDownEvent -= OnBottomButtonDownEvent;
                transform.DOMove(_oldPosition, 1, false);
				StartCoroutine ("CountdownGame");
                    
                if (PagesReadEvent != null)
                {
                    PagesReadEvent();
                }

                return;
            }
            
            _renderer.sprite = _slides[_slideCounter];

            if (PageSwitchedEvent != null)
            {
                PageSwitchedEvent();
            }
        }

		/// <summary>
		/// Countdowns the game.
		/// </summary>
		/// <returns>The game.</returns>
		private IEnumerator CountdownGame()
		{
			Countdown(3);
			yield return new WaitForSeconds (0.8f);
			_countdownTextObject.transform.DOScale(new Vector3(0,0,0),0.2f);
			yield return new WaitForSeconds (0.2f);

			Countdown(2);
			yield return new WaitForSeconds (0.8f);
			_countdownTextObject.transform.DOScale(new Vector3(0,0,0),0.2f);
			yield return new WaitForSeconds (0.2f);

			Countdown(1);
			yield return new WaitForSeconds (0.8f);
			_countdownTextObject.transform.DOScale(new Vector3(0,0,0),0.2f);
			yield return new WaitForSeconds (0.2f);

			//FIGHT
			AudioManager.Instance.Play(_countdownAudio[0],false,true);
			_countdownText.text = "FIGHT";
			StartGame ();
			_countdownTextObject.transform.DOScale(new Vector3(1,1,1),0.8f);
			yield return new WaitForSeconds (0.8f);
			_countdownTextObject.transform.DOScale(new Vector3(0,0,0),0.2f);


		}

		private void Countdown(int number)
		{
			AudioManager.Instance.Play(_countdownAudio[number],false,true);
			_countdownText.text = number.ToString ();
			_countdownTextObject.transform.DOScale(new Vector3(1,1,1),0.8f);
		}

        private void StartGame()
        {
            GameObject.FindObjectOfType<MatchInfo>().StartGame();
        }
    }

}
