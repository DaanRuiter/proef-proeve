﻿using UnityEngine;
using UnityEngine.UI;

public class FloatingTextfield : MonoBehaviour {
    [SerializeField] private Text _text;
    [SerializeField] private int _speed;

    private void Update()
    {
        Color newColor = _text.color;
        newColor.a -= 0.01f;
        _text.color = newColor;

        transform.position += new Vector3(0.5f * _speed, 0.75f * _speed, 0) * Time.deltaTime;

        if (newColor.a <= 0)
            Destroy(this.gameObject);
    }
}
