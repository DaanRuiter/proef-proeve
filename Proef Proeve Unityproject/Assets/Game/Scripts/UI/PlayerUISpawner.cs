﻿using ProefProeve.player;
using ProefProeve.Set;
using UnityEngine;

namespace ProefProeve.UI
{
    /// <summary>
    /// Spawns UI elements for player if active.
    /// </summary>
    [RequireComponent(typeof(Player), typeof(PlayerSetBuilder))]
    public class PlayerUISpawner : MonoBehaviour
    {
        /// <summary>
        /// Maximum amount of controllers supported by the UI resources
        /// </summary>
        public const int MAX_NUM_CONTROLLERS_SUPPORTED = 4;

        private Canvas mainCanvas;

        private void Start()
        {
            DeviceDetector.DetectConnectedDevices();
            int numControllers = DeviceDetector.DevicesConnected;

            if (numControllers < 0 || numControllers > MAX_NUM_CONTROLLERS_SUPPORTED)
            {
                Debug.LogError(string.Format(
                    "Number of controllers is out of supported range: controller amount: {0} > supported amount: {1}", 
                    numControllers, 
                    MAX_NUM_CONTROLLERS_SUPPORTED
                ));
                return;
            }

            Player player = GetComponent<Player>();
            GameObject mainCanvasGameObject = GameObject.FindGameObjectWithTag("MainCanvas") as GameObject;
            mainCanvas = mainCanvasGameObject.RequireComponent<Canvas>();
            
            GameObject setBuilder = SpawnUIElement("UI/Player Set Display-" + player.PlayerID);
            GameObject healthBar = SpawnUIElement("UI/Health Bar-" + player.PlayerID);

            setBuilder.RequireComponent<PlayerSetDisplay>().Init(GetComponent<PlayerSetBuilder>());
            healthBar.RequireComponent<PlayerHealthBar>().Init(player);
        }

        private GameObject SpawnUIElement(string resourcePath)
        {
            GameObject prefab = Resources.Load<GameObject>(resourcePath);

            if (prefab == null)
            {
                Debug.LogError(string.Format("Missing prefab: {0}", resourcePath));
                return null;
            }

            GameObject instance = Instantiate(prefab);
            instance.RequireComponent<RectTransform>().SetParent(mainCanvas.GetComponent<RectTransform>(), false);
            return instance;
        }
    }
}