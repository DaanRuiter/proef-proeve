﻿using System.Collections.Generic;
using ProefProeve.Grid;
using ProefProeve.Set;
using UnityEngine;
using DG.Tweening;
using ProefProeve.player;
using System;
using UnityEngine.UI;

namespace ProefProeve.UI
{
    public class GridPlayerIcon
    {
        public Player Player;
        public Vector3 StartScale;
        public SpriteRenderer IconSpriteRenderer;
        public SpriteRenderer BorderSpriteRenderer;
        public SpriteRenderer BackgroundSpriteRenderer;
        public RawImage PortraitPlayerIcon;
    }


    /// <summary>
    /// Animates the icons for players on the card slot groups
    /// </summary>
	public class CardSlotGridPlayerIconGrid : MonoBehaviour
	{
        [Header("Tween settings")]
        [SerializeField] private float _fadeTweenDuration;
        [SerializeField] private float _scaleTweenDuration;
        [SerializeField] private AnimationCurve _scaleUpAnimationCurve;
        [SerializeField] private Ease _scaleDownTweenEase;

        private SpriteRenderer[] _iconSlots;
        private PlayerSetBuilder[] _setBuilders;
        private Dictionary<int, GridPlayerIcon> _gridPlayerIcons;
        private CardSlotGroup.CardSlotGroupPosition _groupPosition;
        
        public void Init(Player[] players)
        {
            _iconSlots = new SpriteRenderer[transform.childCount];
            for (int i = 0; i < _iconSlots.Length; i++)
            {
                Transform child = transform.GetChild(i);
                if (child.GetComponent<SpriteRenderer>() != null)
                {
                    _iconSlots[i] = child.GetComponent<SpriteRenderer>();
                }
            }
            _setBuilders = new PlayerSetBuilder[players.Length];
            for (int i = 0; i < _setBuilders.Length; i++)
            {
                _setBuilders[i] = players[i].GetComponent<PlayerSetBuilder>();
            }

            _gridPlayerIcons = new Dictionary<int, GridPlayerIcon>();

            Player[] allPlayers = FindObjectsOfType<Player>();        
            _groupPosition = transform.parent.GetComponent<CardSlotGroup>().GridPosition;

            for (int i = 0; i < _iconSlots.Length; i++)
            {
                if (i >= allPlayers.Length)
                {
                    Destroy(_iconSlots[i].gameObject);
                    continue;
                }

                GridPlayerIcon playerIcon = new GridPlayerIcon()
                {
                    Player = allPlayers[i],
                    StartScale = _iconSlots[i].transform.localScale,
                    IconSpriteRenderer = _iconSlots[i],
                    BorderSpriteRenderer = _iconSlots[i].transform.FindChild("Border").GetComponent<SpriteRenderer>(),
                    BackgroundSpriteRenderer = _iconSlots[i].transform.FindChild("Background").GetComponent<SpriteRenderer>(),
                    PortraitPlayerIcon = _iconSlots[i].transform.GetComponentInChildren<RawImage>()
                };
                playerIcon.BorderSpriteRenderer.color = MatchInfo.TeamColors[playerIcon.Player.TeamID];


                _gridPlayerIcons.Add(i, playerIcon);                

                if (_groupPosition != CardSlotGroup.CardSlotGroupPosition.Center)
                {
                    playerIcon.IconSpriteRenderer.DOFade(0, 0);
                    playerIcon.BorderSpriteRenderer.DOFade(0, 0);
                    playerIcon.BackgroundSpriteRenderer.DOFade(0f, 0);
                    playerIcon.PortraitPlayerIcon.DOFade(0f, 0);
                }
            }
            
            for (int i = 0; i < _setBuilders.Length; i++)
            {
                _setBuilders[i].CardSlotGroupSwitchEvent += OnCardSlotGroupSwitched;
            }
        }

        private void OnDestroy()
        {
            for (int i = 0; i < _setBuilders.Length; i++)
            {
                _setBuilders[i].CardSlotGroupSwitchEvent -= OnCardSlotGroupSwitched;
            }
        }

        /// <summary>
        /// Callback for when the player siwtches to a different CardSlotGroup.
        /// </summary>
        /// <param name="playerID">ID of player that switched group.</param>
        /// <param name="previousGroup">Previous group player had selected.</param>
        /// <param name="newGroup">New group player has selected.</param>
        private void OnCardSlotGroupSwitched(int playerID, CardSlotGroup previousGroup, CardSlotGroup newGroup)
        {
            if (newGroup.GridPosition == _groupPosition)
            {
                _gridPlayerIcons[playerID].IconSpriteRenderer.DOFade(1f, _fadeTweenDuration);
                _gridPlayerIcons[playerID].BorderSpriteRenderer.DOFade(1f, _fadeTweenDuration);
                _gridPlayerIcons[playerID].BackgroundSpriteRenderer.DOFade(1f, _fadeTweenDuration);
                _gridPlayerIcons[playerID].PortraitPlayerIcon.DOFade(1f, _fadeTweenDuration);
                _gridPlayerIcons[playerID].IconSpriteRenderer.transform.DOScale(_gridPlayerIcons[playerID].StartScale, _scaleTweenDuration).SetEase(_scaleUpAnimationCurve);
            }
            else if(previousGroup.GridPosition == _groupPosition)
            {
                _gridPlayerIcons[playerID].IconSpriteRenderer.DOFade(0f, _fadeTweenDuration);
                _gridPlayerIcons[playerID].BorderSpriteRenderer.DOFade(0f, _fadeTweenDuration);
                _gridPlayerIcons[playerID].BackgroundSpriteRenderer.DOFade(0f, _fadeTweenDuration);
                _gridPlayerIcons[playerID].PortraitPlayerIcon.DOFade(0f, _fadeTweenDuration);
                _gridPlayerIcons[playerID].IconSpriteRenderer.transform.DOScale(Vector3.zero, _scaleTweenDuration).SetEase(_scaleDownTweenEase);
            }             
        }
    }
}
