﻿using ProefProeve.Set;
using UnityEngine;

namespace ProefProeve.UI
{
    /// <summary>
    /// UI element for a player's set builder.
    /// </summary>
	public class PlayerSetDisplay : MonoBehaviour
	{
        [Header("References")]
        [SerializeField] private GameObject[] _cardSlots;
        [SerializeField] private PlayerSetBuilder _setBuilder;

        /// <summary>
        /// Initialize the display with given builder as listen target.
        /// </summary>
        /// <param name="setBuilder">SetBuilder to add eventlisteners to.</param>
        public void Init(PlayerSetBuilder setBuilder)
        {
            _setBuilder = setBuilder;
            _setBuilder.SetContentChangedEvent += OnSetContentChanged;
            _setBuilder.SetCreatedEvent += OnSetCreated;
        }

        private void OnDestroy()
        {
            _setBuilder.SetContentChangedEvent -= OnSetContentChanged;
            _setBuilder.SetCreatedEvent -= OnSetCreated;
        }
        
        /// <summary>
        /// Callback for when a card has been added or removed to the set.
        /// </summary>
        /// <param name="playerID">Player ID of set owner.</param>
        /// <param name="setCardIndex">Index in set of card slot changed.</param>
        /// <param name="selectedCard">Selected card for slot, = null if card was removed from set.</param>
        private void OnSetContentChanged(int playerID, int setCardIndex, Card selectedCard)
        {
            if (setCardIndex < 0 || setCardIndex >= CardSet.CARDS_REQUIRED_FOR_SET)
                return;
            
            if (selectedCard != null) //card added
            {
                selectedCard.transform.SetParent(_cardSlots[setCardIndex].transform);
                selectedCard.transform.localPosition = Vector3.zero;
                StartCoroutine(selectedCard.GetComponent<Card>().StartShowCardTweensUI());
            }
            else //card removed
            {
                Card[] cardsInSlot = _cardSlots[setCardIndex].GetComponentsInChildren<Card>();
                for (int i = 0; i < cardsInSlot.Length; i++)
                {
                    if (cardsInSlot[i].TweenState != CardTweenState.Down)
                    {
                        StartCoroutine(cardsInSlot[i].StartRemoveCardTweens(
                            _setBuilder.DelayPerCardSeconds,
                            _setBuilder.CardRemovedEndScale,
                            _setBuilder.CardRemovedRotationAdd,
                            _setBuilder.CardRemovedTweenDuration,
                            0));
                    }
                }
            }
        }

        /// <summary>
        /// When a full set has been completed.
        /// </summary>
        /// <param name="playerID">The player that created the set.</param>
        /// <param name="set">The set created by the player.</param>
        private void OnSetCreated(int playerID, CardSet set)
        {
            for (int i = 0; i < _cardSlots.Length; i++)
            {
                StartCoroutine(set.Cards[i].StartRemoveCardTweens(
                    _setBuilder.DelayPerCardSeconds,
                    _setBuilder.CardRemovedEndScale,
                    _setBuilder.CardRemovedRotationAdd,
                    _setBuilder.CardRemovedTweenDuration,
                    i)
                );
            }
        }
	}
}