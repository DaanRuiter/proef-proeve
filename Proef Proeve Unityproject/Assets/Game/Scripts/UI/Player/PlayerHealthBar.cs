﻿using System;
using DG.Tweening;
using ProefProeve.player;
using UnityEngine;
using UnityEngine.UI;

namespace ProefProeve
{
	public class PlayerHealthBar : MonoBehaviour
	{
        [SerializeField] private RectTransform _fillForeground;
        [SerializeField] private Text _HPAmountText;

        private Player _player;
        private Vector2 _foreGroundMaxSize;

        public void Init(Player player)
        {
            _player = player;
            _HPAmountText.text = string.Format("{0}", player.Health);
            _foreGroundMaxSize = _fillForeground.sizeDelta;

            player.OnPlayerHitEvent += OnPlayerHitEventHandler;
            player.OnPlayerHealedEvent += OnPlayerHealedEventHandler;
        }

        private void OnDestroy()
        {
            _player.OnPlayerHitEvent -= OnPlayerHitEventHandler;
            _player.OnPlayerHealedEvent -= OnPlayerHealedEventHandler;
        }

        private void OnPlayerHitEventHandler(Player player, float damage)
        {
            OnPlayerHealedEventHandler(player);
        }

        private void OnPlayerHealedEventHandler(Player player)
        {
            _HPAmountText.DOText(string.Format("{0}", player.Health), 0.5f);

            Vector2 newForegroundSize = _foreGroundMaxSize;
            newForegroundSize.x *= player.Health / (float)player.MaxHealth;

            _fillForeground.DOSizeDelta(newForegroundSize, 0.5f);
        }
    }
}
