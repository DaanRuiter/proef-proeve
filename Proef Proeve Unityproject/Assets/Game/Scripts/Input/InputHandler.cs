﻿using UnityEngine;

namespace ProefProeve
{
    public class InputHandler : MonoBehaviour
    {
        // Consts
        private const string HORIZONTAL_AXIS = "Horizontal-";
        private const string VERTICAL_AXIS = "Vertical-";
        private const string ALT_HORIZONTAL_AXIS = "AltHorizontal-";
        private const string ALT_VERTICAL_AXIS = "AltVertical-";
        private const string ALT_HORIZONTAL_OTHER_AXIS = "AltHorizontalOther-";
        private const string ALT_VERTICAL_OTHER_AXIS = "AltVerticalOther-";

        private const string BOTTOM_BUTTON = "Bottom-";
        private const string TOP_BUTTON = "Top-";
        private const string LEFT_BUTTON = "Left-";
        private const string RIGHT_BUTTON = "Right-";

        private const string LEFT_BUMPER = "LeftBumper-";
        private const string RIGHT_BUMPER = "RightBumper-";

        private const string LEFT_TRIGGER = "LeftTrigger-";
        private const string RIGHT_TRIGGER = "RightTrigger-";
        private const string TRIGGERS = "Triggers-";

        // Delegates and events
        public delegate void OnButtonDownEventHandler(int deviceID);

        // Controller buttons
        public event OnButtonDownEventHandler OnLeftButtonDownEvent;
        public event OnButtonDownEventHandler OnRightButtonDownEvent;
        public event OnButtonDownEventHandler OnTopButtonDownEvent;
        public event OnButtonDownEventHandler OnBottomButtonDownEvent;

        // Controller joysticks
        public event OnButtonDownEventHandler OnHorizontalLeftDownEvent;
        public event OnButtonDownEventHandler OnHorizontalRightDownEvent;
        public event OnButtonDownEventHandler OnVerticalTopButtonDownEvent;
        public event OnButtonDownEventHandler OnVerticalBottomButtonDownEvent;
        public event OnButtonDownEventHandler OnAltHorizontalLeftDownEvent;
        public event OnButtonDownEventHandler OnAltHorizontalRightDownEvent;
        public event OnButtonDownEventHandler OnAltVerticalTopButtonDownEvent;
        public event OnButtonDownEventHandler OnAltVerticalBottomButtonDownEvent;

        // Controller Triggers
        public event OnButtonDownEventHandler OnRightTriggerDownEvent;
        public event OnButtonDownEventHandler OnLeftTriggerDownEvent;
        public event OnButtonDownEventHandler OnTriggersReleasedEvent;

        // Controller Bumper
        public event OnButtonDownEventHandler OnLeftBumperDownEvent;
        public event OnButtonDownEventHandler OnRightBumperDownEvent;

        // Struct
        private struct ControllerInfo
        {
            public string DeviceName;
            public string Horizontal;
            public string Vertical;
            public string AltHorizontal;
            public string AltVertical;

            public string BottomButton;
            public string TopButton;
            public string LeftButton;
            public string RightButton;
            public string LeftBumper;
            public string RightBumper;

            // Left and right trigger seperate for non xbox controllers.
            public string Triggers;
            public string LeftTrigger;
            public string RightTrigger;

            // Is the controller a xbox controller.
            public bool IsXbox;

            // Ondown checkups.
            public bool IsRightTriggerDown;
            public bool IsLeftTriggerDown;
            public bool HasReleaseSignalBeenFired;
        }

        // Variables
        public static InputHandler Instance
        {
            get
            {
                if(_instance == null && GameObject.Find("InputHandler") == null)
                {
                    _instance = new GameObject("InputHandler").AddComponent<InputHandler>();
                }
                else if(_instance == null)
                {
                    _instance = GameObject.Find("InputHandler").GetComponent<InputHandler>();
                }
                return _instance;
            }
        }

        private static InputHandler _instance;
        private ControllerInfo[] _deviceInputs;

        private void Awake()
        {
            Initialize();
        }

        /// <summary>
        /// Initialize this intance, if inputhandler already exists destroy this.
        /// </summary>
        private void Initialize()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else if (_instance == null)
            {
                _instance = this;
            }

            DeviceDetector.OnDevicesLengthChangedHandler += OnDevicesLengthChangedHandler;
            DeviceDetector.DetectConnectedDevices();
        }

        /// <summary>
        /// Triggers when device length has changed inside device detector.
        /// </summary>
        /// <param name="deviceNames"></param>
        private void OnDevicesLengthChangedHandler(string[] deviceNames)
        {
            int length = deviceNames.Length;

            // Cleaning out controller list.
            _deviceInputs = new ControllerInfo[length];
           
            for (int i = 0; i < length; i++)
            {
                // Setting device name.
                _deviceInputs[i].DeviceName = deviceNames[i];

                SetDeviceControlls(i);
            }
        }

        /// <summary>
        /// Sets the controlls for the specified controller with ID.
        /// </summary>
        /// <param name="ID">Controller ID</param>
        private void SetDeviceControlls(int ID)
        {
            int controllerID = ID + 1;
            // Setting controlls.
            _deviceInputs[ID].Horizontal = HORIZONTAL_AXIS + controllerID;
            _deviceInputs[ID].Vertical = VERTICAL_AXIS + controllerID;
            _deviceInputs[ID].AltHorizontal = ALT_HORIZONTAL_AXIS + controllerID;
            _deviceInputs[ID].AltVertical = ALT_VERTICAL_AXIS + controllerID;

            _deviceInputs[ID].TopButton = TOP_BUTTON + controllerID;
            _deviceInputs[ID].BottomButton = BOTTOM_BUTTON + controllerID;
            _deviceInputs[ID].LeftButton = LEFT_BUTTON + controllerID;
            _deviceInputs[ID].RightButton = RIGHT_BUTTON + controllerID;
            _deviceInputs[ID].LeftBumper = LEFT_BUMPER + controllerID;
            _deviceInputs[ID].RightBumper = RIGHT_BUMPER + controllerID;

            // Overriding horizontal and vertical controlls for player 1 so Unity UI will work with the controller inputs.
            if (ID == 0)
            {
                _deviceInputs[ID].Horizontal = HORIZONTAL_AXIS.Substring(0, HORIZONTAL_AXIS.Length - 1);
                _deviceInputs[ID].Vertical = VERTICAL_AXIS.Substring(0, VERTICAL_AXIS.Length - 1);
            }


            // DXInput changes for controllers other then Xbox.
            if (!_deviceInputs[ID].DeviceName.Contains("Xbox"))
            {
                _deviceInputs[ID].BottomButton = RIGHT_BUTTON + controllerID;
                _deviceInputs[ID].LeftButton = BOTTOM_BUTTON + controllerID;
                _deviceInputs[ID].RightButton = LEFT_BUTTON + controllerID;
                _deviceInputs[ID].LeftTrigger = LEFT_TRIGGER + controllerID;
                _deviceInputs[ID].RightTrigger = RIGHT_TRIGGER + controllerID;

                _deviceInputs[ID].AltHorizontal = ALT_HORIZONTAL_OTHER_AXIS + controllerID;
                _deviceInputs[ID].AltVertical = ALT_VERTICAL_OTHER_AXIS + controllerID;

                _deviceInputs[ID].IsXbox = false;
            }
            else
            {
                _deviceInputs[ID].Triggers = TRIGGERS + controllerID;
                _deviceInputs[ID].IsXbox = true;
            }
        }

        private void Update()
        {
            //TODO: Handle ispaused.
            AxisInputs();
            ButtonInputs();
            TriggerInputs();
        }

        /// <summary>
        /// All axis inputs from joysticks.
        /// </summary>
        private void AxisInputs()
        {
            if (_deviceInputs == null)
                return;

            int length = _deviceInputs.Length;
            for (int i = 0; i < length; i++)
            {
                // Left analog stick
                CheckAxis(_deviceInputs[i].Horizontal, i, OnHorizontalRightDownEvent, OnHorizontalLeftDownEvent);
                CheckAxis(_deviceInputs[i].Vertical, i, OnVerticalTopButtonDownEvent, OnVerticalBottomButtonDownEvent);

                // Right analog stick
                CheckAxis(_deviceInputs[i].AltHorizontal, i, OnAltHorizontalRightDownEvent, OnAltHorizontalLeftDownEvent);
                CheckAxis(_deviceInputs[i].AltVertical, i, OnAltVerticalTopButtonDownEvent,  OnAltVerticalBottomButtonDownEvent);
            }
            
        }
        
        /// <summary>
        /// Checks axis and sends event on positive or negative amount of specified axis.
        /// </summary>
        /// <param name="axis">Axis name inside inputmanager.</param>
        /// <param name="ID">Device ID.</param>
        /// <param name="positiveAction">Event to send on positive.</param>
        /// <param name="negativeAction">Event to send on negative.</param>
        private void CheckAxis(string axis, int ID, OnButtonDownEventHandler positiveAction, OnButtonDownEventHandler negativeAction)
        {
            if (Input.GetAxis(axis) > 0.1f) // positive
            {
                if (positiveAction != null)
                    positiveAction(ID);
            }
            if (Input.GetAxis(axis) < -0.1f) // negative
            {
                if (negativeAction != null)
                    negativeAction(ID);
            }
        }

        /// <summary>
        /// All axis inputs from triggers.
        /// </summary>
        private void TriggerInputs()
        {
            int length = _deviceInputs.Length;
            for (int i = 0; i < length; i++)
            {
                if (_deviceInputs[i].IsXbox)
                {
                    XboxTriggers(i);
                }
                else
                {
                    AltTriggers(i);
                }  
            }
        }

        /// <summary>
        /// Checks xbox triggers and sends events on triggers down.
        /// </summary>
        /// <param name="ID">Controller ID</param>
        private void XboxTriggers(int ID)
        {
            if (Input.GetAxis(_deviceInputs[ID].Triggers) < -0.1f) // Right trigger
            {
                if (!_deviceInputs[ID].IsRightTriggerDown)
                {
                    if (OnRightTriggerDownEvent != null)
                        OnRightTriggerDownEvent(ID);

                    _deviceInputs[ID].HasReleaseSignalBeenFired = false;
                }

                _deviceInputs[ID].IsRightTriggerDown = true;
            }
            else
            {
                _deviceInputs[ID].IsRightTriggerDown = false;
            }

            if (Input.GetAxis(_deviceInputs[ID].Triggers) > 0.1f) // Left trigger
            {
                if (!_deviceInputs[ID].IsLeftTriggerDown)
                {
                    if (OnLeftTriggerDownEvent != null)
                        OnLeftTriggerDownEvent(ID);

                    _deviceInputs[ID].HasReleaseSignalBeenFired = false;
                }

                _deviceInputs[ID].IsLeftTriggerDown = true;
            }
            else
            {
                _deviceInputs[ID].IsLeftTriggerDown = false;
            }

            if (!_deviceInputs[ID].IsLeftTriggerDown && !_deviceInputs[ID].IsRightTriggerDown && !_deviceInputs[ID].HasReleaseSignalBeenFired)
            {
                if (OnTriggersReleasedEvent != null)
                    OnTriggersReleasedEvent(ID);

                _deviceInputs[ID].HasReleaseSignalBeenFired = true;
            }
        }

        /// <summary>
        /// Checks alt triggers and sends events on triggers down.
        /// </summary>
        /// <param name="ID">Controller ID</param>
        private void AltTriggers(int ID)
        {
            if (Input.GetAxis(_deviceInputs[ID].RightTrigger) > 0.1f) // Right trigger
            {
                if (!_deviceInputs[ID].IsRightTriggerDown)
                {
                    if (OnRightTriggerDownEvent != null)
                        OnRightTriggerDownEvent(ID);

                    _deviceInputs[ID].HasReleaseSignalBeenFired = false;
                }

                _deviceInputs[ID].IsRightTriggerDown = true;
            }
            else
            {
                _deviceInputs[ID].IsRightTriggerDown = false;
            }

            if (Input.GetAxis(_deviceInputs[ID].LeftTrigger) > 0.1f) // Left Trigger
            {
                if (!_deviceInputs[ID].IsLeftTriggerDown)
                {
                    if (OnLeftTriggerDownEvent != null)
                        OnLeftTriggerDownEvent(ID);

                    _deviceInputs[ID].HasReleaseSignalBeenFired = false;
                }

                _deviceInputs[ID].IsLeftTriggerDown = true;
            }
            else
            {
                _deviceInputs[ID].IsLeftTriggerDown = false;
            }

            if (!_deviceInputs[ID].IsLeftTriggerDown && !_deviceInputs[ID].IsRightTriggerDown && !_deviceInputs[ID].HasReleaseSignalBeenFired)
            {
                if (OnTriggersReleasedEvent != null)
                    OnTriggersReleasedEvent(ID);

                _deviceInputs[ID].IsRightTriggerDown = false;
                _deviceInputs[ID].IsLeftTriggerDown = false;
            }
        }

        /// <summary>
        /// All button inputs from buttons on controller.
        /// </summary>
        private void ButtonInputs()
        {
            int length = _deviceInputs.Length;
            for (int i = 0; i < length; i++)
            {
                // Checking all buttons on controller.
                CheckButtonDown(_deviceInputs[i].BottomButton, i, OnBottomButtonDownEvent);
                CheckButtonDown(_deviceInputs[i].TopButton, i, OnTopButtonDownEvent);
                CheckButtonDown(_deviceInputs[i].LeftButton, i, OnLeftButtonDownEvent);
                CheckButtonDown(_deviceInputs[i].RightButton, i, OnRightButtonDownEvent);
                CheckButtonDown(_deviceInputs[i].LeftBumper, i, OnLeftBumperDownEvent);
                CheckButtonDown(_deviceInputs[i].RightBumper, i, OnRightBumperDownEvent);
            }
        }

        /// <summary>
        /// Checks if button down and sends event on button down.
        /// </summary>
        /// <param name="button">Name of button inside input manager.</param>
        /// <param name="ID">Controller ID</param>
        /// <param name="action">Event to send on button down.</param>
        private void CheckButtonDown(string button, int ID, OnButtonDownEventHandler action)
        {
            if (Input.GetButtonDown(button))
            {
                if (action != null)
                    action(ID);
            }
        }
    }

}
