﻿using UnityEngine;

namespace ProefProeve
{ 
    public class DeviceDetector : MonoBehaviour
    {
        public delegate void OnDevicesLengthChanged(string[] names);
        public static event OnDevicesLengthChanged OnDevicesLengthChangedHandler;
        
        public static int DevicesConnected { get; private set; }

        /// <summary>
        /// Detects all connected devices and sends event if device count has changed in the meantime.
        /// </summary>
        public static void DetectConnectedDevices()
        {
            string[] joystickNames = Input.GetJoystickNames();
            int length = joystickNames.Length;

            if (length != DevicesConnected)
            {
                DevicesConnected = length;
            }

            if(OnDevicesLengthChangedHandler != null)
                OnDevicesLengthChangedHandler(joystickNames);
        }
    }

}
