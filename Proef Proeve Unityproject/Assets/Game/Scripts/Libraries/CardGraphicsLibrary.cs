﻿using ProefProeve.Set;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace ProefProeve
{
    /// <summary>
    /// References 
    /// </summary>
	public class CardGraphicsLibrary : ScriptableObject
	{
        [System.Serializable]
        private struct CardGraphicsLibraryElementEntry
        {
            public CardProperties.Element Element;
            public Sprite Sprite;
        }

        [System.Serializable]
        private struct CardGraphicsLibraryBorderEntry
        {
            public CardProperties.Grade Grade;
            public Sprite Sprite;
        }

        [System.Serializable]
        private struct CardGraphicsLibraryBackgroundEntry
        {
            public CardProperties.Color Color;
            public Sprite Sprite;
        }
        
        [SerializeField] private CardGraphicsLibraryElementEntry[] _elementalSprites;
        [SerializeField] private CardGraphicsLibraryBorderEntry[] _borderSprites;
        [SerializeField] private CardGraphicsLibraryBackgroundEntry[] _backgroundSprites;

#if UNITY_EDITOR
        [MenuItem("Create/CardGraphicsLibrary")]
        public static void CreatInstance()
        {
            ScriptableObjectUtility.CreateAsset<CardGraphicsLibrary>();
        }
#endif

        /// <summary>
        /// Returns the sprite assigned in the library asset of the given element
        /// </summary>
        /// <returns>Sprite of the element if specified in the library else returns null</returns>
        public Sprite GetSpriteForElement(CardProperties.Element element)
        {
            for (int i = 0; i < _elementalSprites.Length; i++)
            {
                if (_elementalSprites[i].Element == element)
                {
                    return _elementalSprites[i].Sprite;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns the border sprite assigned in the library asset of the given grade
        /// </summary>
        /// <returns>Sprite of the border if specified in the library else returns null</returns>
        public Sprite GetBorderForGrade(CardProperties.Grade grade)
        {
            for (int i = 0; i < _borderSprites.Length; i++)
            {
                if (_borderSprites[i].Grade == grade)
                {
                    return _borderSprites[i].Sprite;
                }
            }
            return null;
        }
        
        public Sprite GetBackgroundForColor(CardProperties.Color color)
        {
            for (int i = 0; i < _backgroundSprites.Length; i++)
            {
                if (_backgroundSprites[i].Color == color)
                {
                    return _backgroundSprites[i].Sprite;
                }
            }
            return null;
        }
    }
}
