﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace ProefProeve
{
	public class ScreenTransition : MonoBehaviour
	{
        public static ScreenTransition Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = (Instantiate(Resources.Load("UI/Screen Transition Canvas")) as GameObject).RequireComponent<ScreenTransition>();
                    DontDestroyOnLoad(_instance.gameObject);
                }
                return _instance;
            }
        }
        private static ScreenTransition _instance;

        public Color OverlayColor
        {
            get
            {
                return _overlayImage.color;
            }
            set
            {
                _overlayImage.color = value;
            }
        }

        public float TimeLeftInCurrectFade
        {
            get
            {
                if (_imageFadeTweener == null)
                    return 0;
                return _imageFadeTweener.Duration() - _imageFadeTweener.Elapsed();
            }
        }

        private Image _overlayImage;
        private Tweener _imageFadeTweener;

        public void FadeTo(float endAlpha, float duration, TweenCallback onCompletedCallback = null)
        {
            if (_imageFadeTweener != null)
                _imageFadeTweener.Kill();

            _imageFadeTweener = _overlayImage.DOFade(endAlpha, duration).OnComplete(onCompletedCallback).SetEase(Ease.Linear);
        }

        private void Awake()
        {
            _overlayImage = transform.GetComponentInChildren<Image>();
        }
    }
}
