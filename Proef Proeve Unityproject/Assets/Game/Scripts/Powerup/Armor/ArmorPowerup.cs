﻿using UnityEngine;

namespace ProefProeve.Powerup
{
    /// <summary>
    /// Powerup for armor see refrence Armor.
    /// </summary>
    public class ArmorPowerup : UseablePowerup
    {
        private const int HITS_TO_DEFEND = 1;

        public ArmorPowerup()
        {
            isBuff = true;
        }

        public override void Use(player.Player player)
        {
            if(!player.GetComponent<Armor>())
            {
                Armor effect = player.gameObject.AddComponent<Armor>();
                effect.Initialize(HITS_TO_DEFEND);
                player.RegisterEffect(effect);
            }
        }

    }

}
