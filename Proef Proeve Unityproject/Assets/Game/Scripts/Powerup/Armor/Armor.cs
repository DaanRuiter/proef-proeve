﻿using UnityEngine;
namespace ProefProeve.Powerup
{
    public class Armor : PowerupEffect
    {
        private GameObject _animation;
        private int _hitsToDefend = 0;

        public void Initialize(int hitsToDefend)
        {
            _hitsToDefend = hitsToDefend;
        }

        public void TakeHit()
        {
            _hitsToDefend--;
            if(_hitsToDefend <= 0)
            {
                RemoveMe();
            }
        }

        public void SetEffect(GameObject obj)
        {
            _animation = obj;
        }

        protected override void RemoveMe()
        {
            Destroy(_animation);
            base.RemoveMe();
        }

    }
}
