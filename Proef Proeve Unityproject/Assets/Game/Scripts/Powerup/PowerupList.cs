﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace ProefProeve.Powerup
{
    public class PowerupList : ScriptableObject
    { 
        [System.Serializable]
        private struct PowerupElement
        {
            [TypeDropdown(typeof(UseablePowerup))]public string Powerup;
            public Sprite Icon;
        }

        /// <summary>
        /// All powerups in asset.
        /// </summary>
        public UseablePowerup[] List
        {
            get
            {
                int length = _list.Length;
                UseablePowerup[] result = new UseablePowerup[length];
                for (int i = 0; i < length; i++)
                {
                    var type = System.Type.GetType(_list[i].Powerup);
                    var instance = System.Activator.CreateInstance(type);
                    result[i] = instance as UseablePowerup;
                }

                return result;
            }
        }

        /// <summary>
        /// All powerup icons in asset.
        /// </summary>
        public Sprite[] Icons
        {
            get
            {
                int length = _list.Length;
                Sprite[] sprites = new Sprite[length];
                for (int i = 0; i < length; i++)
                {
                    sprites[i] = _list[i].Icon;
                }

                return sprites;
            }
        }

        public float IconScale
        {
            get
            {
                return _iconScale;
            }
        }

        public float IconYOffset
        {
            get
            {
                return _iconYOffset;
            }
        }
        
        [SerializeField] private PowerupElement[] _list;
        [SerializeField] private float _iconScale;
        [SerializeField] private float _iconYOffset;

#if UNITY_EDITOR
        [MenuItem("Powerups/Create New PowerupList")]
        public static void CreateInstance()
        {
            PowerupList obj = ScriptableObject.CreateInstance<PowerupList>();

            obj.name = "New PowerupList";

            AssetDatabase.CreateAsset(obj, "Assets/PowerupList.asset");

            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();

            Selection.activeObject = obj;
        }
#endif

    }

}
