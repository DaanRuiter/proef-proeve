﻿namespace ProefProeve.Powerup
{
    public class Heal : PowerupEffect
    {

        public float Modifier
        {
            get
            {
                return _healModifier;
            }
        }

        private float _healModifier;

        public void Initialize(float modifier)
        {
            _healModifier = modifier;
        }
    }

}
