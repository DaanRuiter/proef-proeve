﻿namespace ProefProeve.Powerup
{
    public class HealPowerup : UseablePowerup
    {
        private const float HEAL_MODIFIER = 1;

        public HealPowerup()
        {
            isBuff = true;
        }

        public override void Use(player.Player player)
        {
            Heal effect = player.gameObject.AddComponent<Heal>();
            effect.Initialize(HEAL_MODIFIER);
            player.RegisterEffect(effect);
        }
    }

}