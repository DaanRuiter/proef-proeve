﻿using UnityEngine;

namespace ProefProeve.Powerup
{
    public abstract class PowerupEffect : MonoBehaviour
    {
        public delegate void EffectRemoved(PowerupEffect effect);
        public event EffectRemoved EffectRemovedHandler;

        /// <summary>
        /// Sends a event that the effect is removed and destroys itself.
        /// </summary>
        protected virtual void RemoveMe()
        {
            if (EffectRemovedHandler != null)
                EffectRemovedHandler(this);

            Destroy(this);
        }
    }

}