﻿namespace ProefProeve.Powerup
{
    public class CriticalPowerup : UseablePowerup
    {
        private const float CRITICAL_MODIFIER = 0.5f;

        public CriticalPowerup()
        {
            isBuff = true;
        }

        public override void Use(player.Player player)
        {
            Critical effect = player.gameObject.AddComponent<Critical>();
            effect.Initialize(CRITICAL_MODIFIER);
            player.RegisterEffect(effect);
        }

        private void Awake()
        {
            
        }
    }

}