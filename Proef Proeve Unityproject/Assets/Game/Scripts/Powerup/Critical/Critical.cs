﻿namespace ProefProeve.Powerup
{
    public class Critical : PowerupEffect
    {

        public float Modifier
        {
            get
            {
                return _damageModifier;
            }
        }

        private float _damageModifier;

        public void Initialize(float modifier)
        {
            _damageModifier = modifier;
        }
    }

}