﻿using UnityEngine;
namespace ProefProeve.Powerup
{
    public abstract class UseablePowerup
    {
        public bool IsBuff
        {
            get
            {
                return isBuff;
            }
        }

        protected bool isBuff;

        public abstract void Use(player.Player target);
    }

}
