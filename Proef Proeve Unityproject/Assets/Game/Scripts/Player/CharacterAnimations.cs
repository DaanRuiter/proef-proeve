﻿public static class CharacterAnimations {
    public const string BASIC_ATTACK = "Attack 1";
    public const string SPECIAL_ATTACK = "Attack 2";
    public const string DEATH = "Death";
    public const string HIT = "Hit";
    public const string IDLE = "Idle";
    public const string STUN = "Stunned";
}
