﻿using UnityEngine;
using DG.Tweening;

namespace ProefProeve.player
{
    [RequireComponent(typeof(Player))]
    public class PlayerTarget : MonoBehaviour {
        private const float TIME_VISIBLE = 2;

        public enum PlayerPosition
        {
            LeftTop = 0,
            RightTop,
            LeftBottom,
            RightBottom
        }

        /// <summary>
        /// Current player targeted by this instance.
        /// </summary>
        public Player Target
        {
            get
            {
                return _matchInfo.GetPlayerFromPosition(_targetPosition);
            }
        }

        /// <summary>
        /// Current position targeted by this instance.
        /// </summary>
        public PlayerPosition TargetPosition
        {
            get
            {
                return _targetPosition;
            }
        }

        private SpriteRenderer _targetSpriteRenderer;
        [SerializeField] private Transform _targetPointer;

        private PlayerPosition _targetPosition;
        private PlayerPosition _oldTargetPosition;

        private MatchInfo _matchInfo;
        private Player _myPlayer;
        private Color _startColor;

        private Tweener _fadeTweener;

        public void SetMatchInfo(MatchInfo matchInfo, PlayerPosition pos)
        {
            _matchInfo = matchInfo;

            // Target pos now here but should have its own spot.
            _oldTargetPosition = pos;
            _targetPosition = pos;
        }

        public void Init(Player player)
        {
            _myPlayer = player;
            _targetPointer = GetComponentInChildren<TargetPointerTag>().transform;
            _targetSpriteRenderer = _targetPointer.GetComponentInChildren<SpriteRenderer>();
            _startColor = _targetSpriteRenderer.color;

            RegisterEvents();

            ShowPointer(0);
        }

        private void OnDisable()
        {
            UnRegisterEvents();
        }
        
        private void RegisterEvents()
        {
            InputHandler inputHandler = InputHandler.Instance;
            // Horizontal inputs.
            inputHandler.OnAltHorizontalLeftDownEvent += OnHorizontalLeftDownHandler;
            inputHandler.OnAltHorizontalRightDownEvent += OnHorizontalRightDownHandler;
            inputHandler.OnHorizontalLeftDownEvent += OnHorizontalLeftDownHandler;
            inputHandler.OnHorizontalRightDownEvent += OnHorizontalRightDownHandler;

            // Vertical inputs.
            inputHandler.OnAltVerticalBottomButtonDownEvent += OnVerticalBottomButtonDownHandler;
            inputHandler.OnAltVerticalTopButtonDownEvent += OnVerticalTopButtonDownHandler;
            inputHandler.OnVerticalTopButtonDownEvent += OnVerticalTopButtonDownHandler;
            inputHandler.OnVerticalBottomButtonDownEvent += OnVerticalTopButtonDownHandler;
        }

        private void UnRegisterEvents()
        {
            InputHandler inputHandler = InputHandler.Instance;
            // Horizontal inputs.
            inputHandler.OnAltHorizontalLeftDownEvent -= OnHorizontalLeftDownHandler;
            inputHandler.OnAltHorizontalRightDownEvent -= OnHorizontalRightDownHandler;
            inputHandler.OnHorizontalLeftDownEvent -= OnHorizontalLeftDownHandler;
            inputHandler.OnHorizontalRightDownEvent -= OnHorizontalRightDownHandler;

            // Vertical inputs.
            inputHandler.OnAltVerticalBottomButtonDownEvent -= OnVerticalBottomButtonDownHandler;
            inputHandler.OnAltVerticalTopButtonDownEvent -= OnVerticalTopButtonDownHandler;
            inputHandler.OnVerticalTopButtonDownEvent -= OnVerticalTopButtonDownHandler;
            inputHandler.OnVerticalBottomButtonDownEvent -= OnVerticalTopButtonDownHandler;
        }

        /// <summary>
        /// Moves the target position towards new position.
        /// </summary>
        /// <param name="standardPositionOne">Start position links to movePositionOne</param>
        /// <param name="movePositionOne">Move to position one</param>
        /// <param name="standardPositionTwo">Start position links to movePositionTwo</param>
        /// <param name="movePositionTwo">Move to position two</param>
        private void MoveToPosition(PlayerPosition standardPositionOne, PlayerPosition movePositionOne, PlayerPosition standardPositionTwo, PlayerPosition movePositionTwo)
        {
            if (_targetPosition == standardPositionOne)
            {
                _targetPosition = movePositionOne;
            }
            else if (_targetPosition == standardPositionTwo)
            {
                _targetPosition = movePositionTwo;
            }

            CheckTargetPosition();
        }

        private void OnVerticalTopButtonDownHandler(int deviceID)
        {
            if(_myPlayer.ControllerID == deviceID)
                MoveToPosition(PlayerPosition.LeftBottom, PlayerPosition.LeftTop, PlayerPosition.RightBottom, PlayerPosition.RightTop);
        }

        private void OnVerticalBottomButtonDownHandler(int deviceID)
        {
            if (_myPlayer.ControllerID == deviceID)
                MoveToPosition(PlayerPosition.LeftTop, PlayerPosition.LeftBottom, PlayerPosition.RightTop, PlayerPosition.RightBottom);
        }

        private void OnHorizontalRightDownHandler(int deviceID)
        {
            if (_myPlayer.ControllerID == deviceID)
                MoveToPosition(PlayerPosition.LeftTop, PlayerPosition.RightTop, PlayerPosition.LeftBottom, PlayerPosition.RightBottom);
        }

        private void OnHorizontalLeftDownHandler(int deviceID)
        {
            if (_myPlayer.ControllerID == deviceID)
                MoveToPosition(PlayerPosition.RightTop, PlayerPosition.LeftTop, PlayerPosition.RightBottom, PlayerPosition.LeftBottom);
        }

        /// <summary>
        /// Checks if target position is a valid position.
        /// </summary>
        private void CheckTargetPosition()
        {
            Player targettedPlayer = _matchInfo.GetPlayerFromPosition(_targetPosition);
            if (targettedPlayer == null || targettedPlayer == _myPlayer)
            {
                _targetPosition = _oldTargetPosition;
            }
            else
            {
                // Targetting by rotating the pointer.
                Vector3 dir = targettedPlayer.transform.position - _targetPointer.position;
                float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                _targetPointer.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

                _oldTargetPosition = _targetPosition;
            }

            ShowPointer(TIME_VISIBLE);
        }

        /// <summary>
        /// Shows pointer for a specific amount of time.
        /// </summary>
        private void ShowPointer(float showTime)
        {
            if (_fadeTweener != null)
                _fadeTweener.Kill();

            _targetSpriteRenderer.color = _startColor;

            // Starting fade with time that sprite should be shown.
            _fadeTweener = _targetSpriteRenderer.DOFade(0, showTime);
        }
    }
}
