﻿using UnityEngine;
using System.Collections.Generic;
using ProefProeve.Powerup;
using ProefProeve.Set;
using ProefProeve.Animation;
using ProefProeve.Grid;

namespace ProefProeve.player
{
    /// <summary>
    /// Data model for keeping track for player stats
    /// </summary>
    public struct PlayerStats
    {
        public int DamageDone;
        public int SetsCompleted;
        public int PowerupsUsed;
    }

    /// <summary>
    /// Main 
    /// </summary>
	public class Player : MonoBehaviour 
	{
        public const int PLAYER_STARTING_HP = 150;
        public const float PLAYER_STUN_DURATION_SEC = 1.5f;

        public delegate void OnPlayerPowerupRegisteredEventHandler(Player player, Powerup.PowerupEffect effect);
        public event OnPlayerPowerupRegisteredEventHandler OnPlayerPowerupRegisteredEvent;

        public delegate void OnPlayerHitEventHandler(Player player, float damage);
        public event OnPlayerHitEventHandler OnPlayerHitEvent;

        public delegate void OnPlayerActionEventHandler(Player player);
        public event OnPlayerActionEventHandler OnPlayerCriticalHitEvent;
        public event OnPlayerActionEventHandler OnPlayerHealedEvent;

        public delegate void OnPlayerAttackedEventHandler(Player player, Player target, CardProperties.Element element);
        public event OnPlayerAttackedEventHandler OnPlayerAttackedEvent;

        public int PlayerID { get; private set; }
        public int ControllerID { get; private set; }        
        public int TeamID { get; private set; }
        public int Health { get { return _playerHealth; } }
        public int MaxHealth { get { return _maxPlayerHealth; } }
        public bool IsAlive { get { return _playerHealth > 0; } }
        public int CharacterID { get; private set; }

        public PlayerTarget PlayerTarget
        {
            get
            {
                return _playerTarget;
            }
        }

        public PlayerStats Stats
        {
            get
            {
                return _stats;
            }
        }

        public bool IsStunned
        {
            get
            {
                return _isStunned;
            }
        }

        [Header("Debug")]
        [SerializeField] private bool _debugIdsEnabled;
        [SerializeField] private int _debugPlayerID;
        [SerializeField] private int _debugControllerID;
        [SerializeField] private int _debugCharacterID;
        [SerializeField] private int _debugTeamID;

        private int _playerHealth;
		private int _maxPlayerHealth;
        private bool _isStunned;

        private PlayerStats _stats;

        private PlayerSetBuilder _setBuilder;
        private PlayerTarget _playerTarget;
        private AnimationController _animationController;
        private List<PowerupEffect> _powerupEffects = new List<PowerupEffect>();

        /// <summary>
        /// Initialize player with given IDs.
        /// </summary>
        /// <param name="playerID">Unique player ID.</param>
        /// <param name="controllerID">ID of controller used by player.</param>
        /// <param name="teamID">ID of player's team, -1 = no team.</param>
        public void Init(int playerID, int controllerID, int characterID = 0, int teamID = -1)
        { 
            PlayerID = playerID;
            ControllerID = controllerID;
            CharacterID = characterID;
            TeamID = teamID;

            _playerHealth = PLAYER_STARTING_HP;
            _maxPlayerHealth = _playerHealth;

            ResetPlayerStats();
        }

        /// <summary>
        /// Initialize components used in gameplay
        /// </summary>
        public void InitComponents(CardSlotGrid grid)
        {
            _setBuilder = gameObject.RequireComponent<PlayerSetBuilder>();
            _setBuilder.Init(grid);
            _setBuilder.SetCreatedEvent += SetCreatedEventHandler;
            
            _playerTarget = gameObject.RequireComponent<PlayerTarget>();
            _playerTarget.Init(this);            

            _animationController = GetComponentInChildren<AnimationController>();
            _animationController.Play(CharacterAnimations.IDLE);
        }

        /// <summary>
        /// Changes the health.
        /// </summary>
        /// <param name="amount">Amount.</param>
        public void ChangeHealth(int amount)
		{
            if(GetArmor() != null)
            {
                GetArmor().TakeHit();
                return;
            }
            
            _playerHealth = Mathf.Clamp(_playerHealth + amount, 0, _maxPlayerHealth);

            if(amount < 0)
            {
                if (OnPlayerHitEvent != null)
                    OnPlayerHitEvent(this, -amount);

                _animationController.Play(CharacterAnimations.HIT);
            }

			if (_playerHealth <= 0) 
			{
				Die ();
			}
		}

        /// <summary>
        /// Registers powerup effect to this instance.
        /// </summary>
        /// <param name="effect">EFfect that registers to this instance.</param>
        public void RegisterEffect(PowerupEffect effect)
        {
            _powerupEffects.Add(effect);
            effect.EffectRemovedHandler += RemoveEffect;

            if (OnPlayerPowerupRegisteredEvent != null)
                OnPlayerPowerupRegisteredEvent(this, effect);
        }

        private void Awake()
        {
            if (_debugIdsEnabled)
            {
                Init(_debugPlayerID, _debugControllerID, _debugCharacterID,_debugTeamID);
            }
        }

        /// <summary>
        /// Triggers on player set created.
        /// </summary>
        /// <param name="playerID"></param>
        /// <param name="set"></param>
        private void SetCreatedEventHandler(int playerID, CardSet set)
        {
            if (set.IsValid)
            {
                _stats.SetsCompleted++;

                _animationController.Play(CharacterAnimations.BASIC_ATTACK);

                Card[] setCards = set.Cards;
                float damageModifier = 1;
                float healModifier = 0;
                int totalDamage = 0;
                int totalHeal = 0;

                int length = setCards.Length;
                for (int i = 0; i < length; i++)
                {
                    totalDamage += setCards[i].Damage;

                    if (setCards[i].UseablePowerup != null)
                    {
                        FirePowerup(setCards[i].UseablePowerup);
                    }
                }

                PowerupEffect[] effects = GetComponents<PowerupEffect>();
                damageModifier = GetCriticalModifier(effects);
                healModifier = GetHealModifier(effects);

                totalDamage = Mathf.FloorToInt(totalDamage * damageModifier);
                totalHeal = Mathf.FloorToInt(totalDamage * healModifier);

                bool critical = (damageModifier > 1);
                Attack(_playerTarget.Target, totalDamage, setCards[0].Properties.CardElement, critical);

                if(totalHeal > 0)
                    Heal(this, totalHeal);
            }
            else
            {
                _animationController.Play(CharacterAnimations.STUN);
                _isStunned = true;

                Invoke("DisableStun", PLAYER_STUN_DURATION_SEC);
            }
        }

        /// <summary>
        /// Attacks targeted player with amount of damage also adds damage multiplier.
        /// </summary>
        private void Attack(Player target, int damage, CardProperties.Element element, bool critical)
        {
            _stats.DamageDone += damage;

            target.ChangeHealth(-damage);
            
            if (critical && OnPlayerCriticalHitEvent != null)
                OnPlayerCriticalHitEvent(target);

            if (OnPlayerAttackedEvent != null)
            {
                OnPlayerAttackedEvent(this, target, element);
            }
        }

        /// <summary>
        /// Heals the target for amount of heal.
        /// </summary>
        private void Heal(Player target, int heal)
        {
            target.ChangeHealth(heal);

            if(OnPlayerHealedEvent != null)
            {
                OnPlayerHealedEvent(this);
            }
        }

        private float GetCriticalModifier(PowerupEffect[] effects)
        {
            float damageModifier = 1;
            foreach (PowerupEffect effect in effects)
            {
                if (effect is Critical)
                {
                    Critical criticalPowerup = (Critical)effect;
                    damageModifier += criticalPowerup.Modifier;
                    Destroy(effect);
                }
            }

            return damageModifier;
        }

        private int GetHealModifier(PowerupEffect[] effects)
        {
            int result = 0;
            foreach (PowerupEffect effect in effects)
            {
                if (effect is Heal)
                {
                    result++;
                    Destroy(effect);
                }
            }

            return result;
        }

        /// <summary>
        /// Fires powerup found in card.
        /// </summary>
        private void FirePowerup(UseablePowerup powerup)
        {
            _stats.PowerupsUsed++;

            // If powerup is a buff trigger on self.
            if (powerup.IsBuff)
            {
                powerup.Use(this);
                return;
            }

            // Else trigger on targeted player.
            Player target = _playerTarget.Target;
            powerup.Use(target);

            _animationController.PlayImmediate(CharacterAnimations.SPECIAL_ATTACK);
        }

        /// <summary>
        /// Removes powerup effect on this instance.
        /// </summary>
        /// <param name="effect">Effect that has to be removed.</param>
        private void RemoveEffect(PowerupEffect effect)
        {
            _powerupEffects.Remove(effect);
        }

        /// <summary>
        /// Gets the player armor if exists.
        /// </summary>
        /// <returns>Armor of player if exists; otherwise returns null.</returns>
        private Armor GetArmor()
        {
            int length = _powerupEffects.Count;
            for (int i = 0; i < length; i++)
            {
                if (_powerupEffects[i] is Armor)
                {
                    Armor playerArmor = (Armor)_powerupEffects[i];
                    return playerArmor;
                }
            }

            return null;
        }

		/// <summary>
		/// Resets the player stats.
		/// </summary>
		private void ResetPlayerStats()
		{
			_playerHealth = _maxPlayerHealth;
            _stats = new PlayerStats();
		}

		/// <summary>
		/// The players dies.
		/// </summary>
		private void Die()
		{
            _animationController.PlayImmediate(CharacterAnimations.DEATH);
        }

        private void DisableStun()
        {
            _animationController.PlayImmediate(CharacterAnimations.IDLE);
            _isStunned = false;
        }
	}
}
