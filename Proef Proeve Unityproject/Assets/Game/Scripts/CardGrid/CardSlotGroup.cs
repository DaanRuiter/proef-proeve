﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProefProeve.Grid
{
    /// <summary>
    /// A group of 4 cards on the grid
    /// </summary>
	public class CardSlotGroup : MonoBehaviour
    {
        /// <summary>
        /// The position of the card slot group on the grid
        /// </summary>
        public enum CardSlotGroupPosition
        {
            Left,
            Center,
            Right
        }

        /// <summary>
        /// The position of the group on the grid
        /// </summary>
        public CardSlotGroupPosition GridPosition
        {
            get
            {
                return _gridPosition;
            }
        }

        /// <summary>
        /// The card slots in this group
        /// </summary>
        public CardSlot[] CardSlots { get; private set; }

        [SerializeField] private CardSlotGroupPosition _gridPosition;

        private void Awake()
        {
            CardSlots = gameObject.GetComponentsInChildren<CardSlot>();
        }
	}
}
