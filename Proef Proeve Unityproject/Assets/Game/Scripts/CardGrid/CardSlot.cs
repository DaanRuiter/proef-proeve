﻿using System.Collections;
using System.Collections.Generic;
using ProefProeve.Set;
using UnityEngine;

namespace ProefProeve.Grid
{
    /// <summary>
    /// A slot that fits a single card on the grid
    /// </summary>
	public class CardSlot : MonoBehaviour
	{
        public Card Card
        {
            get
            {
                return _cardInSlot;
            }
        }

        private Card _cardInSlot;
        
        /// <summary>
        /// Set the card that currently is filling the slot
        /// </summary>
        /// <param name="card">card to fill the slot with</param>
        /// <param name="destroyCurrentCard">Should the old card be destroyed if there is one?</param>
        public void SetCard(Card card, bool destroyCurrentCard = true)
        {
            if (destroyCurrentCard && _cardInSlot != null)
            {
                Destroy(_cardInSlot.gameObject);
            }
            _cardInSlot = card;
        }
	}
}
