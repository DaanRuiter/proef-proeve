﻿using System.Collections;
using System.Collections.Generic;
using ProefProeve.Set;
using ProefProeve.UI;
using UnityEngine;

namespace ProefProeve.Grid
{
    /// <summary>
    /// The entire card grid
    /// </summary>
	public class CardSlotGrid : MonoBehaviour
	{        
        public Dictionary<CardSlotGroup.CardSlotGroupPosition, CardSlotGroup> CardSlotGroups
        {
            get
            {
                return _cardSlotGroups;
            }
        }

        /// <summary>
        /// Max amount of pickups that can exist inside the grid.
        /// </summary>
        private const int MAX_PICKUPS = 3;

        /// <summary>
        /// Chance to drop a pickup on a card.
        /// </summary>
        private const int PICKUP_DROP_CHANCE = 10;

        [Header("Tween settings")]
        [SerializeField] private float _cardRemovedEndScale;
        [SerializeField] private float _cardRemovedTweenDuration;
        [SerializeField] private Vector3 _cardRemovedRotationAdd;
        [SerializeField] private float _delayPerCardSeconds;

        private Dictionary<CardSlotGroup.CardSlotGroupPosition, CardSlotGroup> _cardSlotGroups;

        private int _pickupsOnGrid = 0;

        /// <summary>
        /// Generates cards in the grid and destroys the old ones
        /// </summary>
        public void GenerateGrid()
        {
            CardSlot[] allSlots = GetAllCardSlots();
            List<int> emptySlots = new List<int>();
            for (int i = 0; i < allSlots.Length; i++)
            {
                emptySlots.Add(i);
            }

            //Insert set so there is always a set at the start
            bool powerupSet = false;
            CardProperties[] set = CardHelper.GenerateRandomSetProperties();
            for (int i = 0; i < set.Length; i++)
            {
                GameObject card = CardHelper.CreateCard(set[i]);
                int slotIndex = Random.Range(0, emptySlots.Count);
                StartCoroutine(card.GetComponent<Card>().StartShowCardTweens(_delayPerCardSeconds, _cardRemovedTweenDuration, slotIndex));

                InsertCard(card, allSlots[emptySlots[slotIndex]]);
                emptySlots.RemoveAt(slotIndex);

                if(!powerupSet)
                {
                    AddPickupToCard(card);
                    powerupSet = true;
                }
            }

            //Fill remaining slots
            for (int i = 0; i < emptySlots.Count; i++)
            {
                int slotIndex = emptySlots[i];
                RandomizeCardAt(slotIndex, slotIndex);
            }
        }

        /// <summary>
        /// Spawn a randomized card at the slot with the given index
        /// </summary>
        /// <param name="gridIndex"></param>
        /// <param name="delayMultiplier"></param>
        public void RandomizeCardAt(int gridIndex, int delayMultiplier = 0)
        {
            CardSlot[] slots = GetAllCardSlots();

            Card card = slots[gridIndex].Card;
            if (card != null)
            {
                if (card.UseablePowerup != null)
                    _pickupsOnGrid--;

                StartCoroutine(card.StartRemoveCardTweens(_delayPerCardSeconds, _cardRemovedEndScale, _cardRemovedRotationAdd, _cardRemovedTweenDuration, 0, delegate ()
                {
                    StartCoroutine(CreateRandomCardAt(gridIndex).StartShowCardTweens(_delayPerCardSeconds, _cardRemovedTweenDuration, delayMultiplier));
                }));
            }
            else
            {
                StartCoroutine(CreateRandomCardAt(gridIndex).StartShowCardTweens(_delayPerCardSeconds, _cardRemovedTweenDuration, delayMultiplier));
            }
        }
        
        public CardSlotGroup GetSlotGroup(CardSlotGroup.CardSlotGroupPosition position)
        {
            return _cardSlotGroups[position];
        }

        private IEnumerator Start()
        {
            CardSlotGroup[] groups = gameObject.GetComponentsInChildren<CardSlotGroup>();

            _cardSlotGroups = new Dictionary<CardSlotGroup.CardSlotGroupPosition, CardSlotGroup>();
            for (int i = 0; i < groups.Length; i++)
            {
                CardSlotGroup group = groups[i];
                group.name += string.Format(" {0}", group.GridPosition);

                _cardSlotGroups.Add(group.GridPosition, group);
            }

            GameRulesPanel rulesPanel = FindObjectOfType<GameRulesPanel>();
            if (rulesPanel != null)
            {
                rulesPanel.PagesReadEvent += delegate ()
                {
                    GenerateGrid();
                };
            }
            else
            {
                yield return new WaitForSeconds(ScreenTransition.Instance.TimeLeftInCurrectFade);
                GenerateGrid();
            }
        }

        private void InsertCard(GameObject card, CardSlot slot)
        {
            CardSlot[] allSlots = GetAllCardSlots();
            
            card.transform.SetParent(slot.transform);
            card.transform.localPosition = Vector3.zero;
            card.GetComponent<Card>().GridIndex = System.Array.IndexOf(allSlots, slot);
            slot.SetCard(card.GetComponent<Card>());
        }

        private CardSlot[] GetAllCardSlots()
        {
            List<CardSlot> slots = new List<CardSlot>();
            foreach (KeyValuePair<CardSlotGroup.CardSlotGroupPosition, CardSlotGroup> group in _cardSlotGroups)
            {
                slots.AddRange(group.Value.CardSlots);
            }
            return slots.ToArray();
        }

        private Card[] GetAllCardsInGrid()
        {
            CardSlot[] allSlots = GetAllCardSlots();
            Card[] cards = new Card[allSlots.Length];
            for (int i = 0; i < allSlots.Length; i++)
            {
                cards[i] = allSlots[i].Card;
            }
            return cards;
        }

        private Card CreateRandomCardAt(int gridIndex)
        {
            CardSlot[] slots = GetAllCardSlots();
            CardProperties properties = CardProperties.Random;
            GameObject cardGameObject = CardHelper.CreateCard(properties);
            InsertCard(cardGameObject, slots[gridIndex]);

            if(_pickupsOnGrid < MAX_PICKUPS)
            {
                int chance = Random.Range(0, 100);
                if (chance >= PICKUP_DROP_CHANCE)
                {
                    AddPickupToCard(cardGameObject);
                }
            }

            return cardGameObject.GetComponent<Card>();
        }

        private void AddPickupToCard(GameObject card)
        {
            Card cardClass = card.GetComponent<Card>();
            CardHelper.SetRandomPickup(cardClass);
            _pickupsOnGrid++;
        }
    }
}
