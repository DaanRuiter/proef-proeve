﻿using System.Collections.Generic;
using DG.Tweening;
using ProefProeve.Grid;
using ProefProeve.player;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ProefProeve.Audio;

namespace ProefProeve
{
	public class GameStateTracker : MonoBehaviour
	{
        [SerializeField] private Canvas _UIPopUpCanvas;
        [SerializeField] private EventSystem _eventSystem;
        [SerializeField] private CardSlotGrid _cardGrid;
		[SerializeField] private AudioAsset _gameOverSound;

        private MatchInfo _matchInfo;
        private GameInitializer _gameInitializer;

        public void Init(MatchInfo matchInfo)
        {
            _gameInitializer = GetComponent<GameInitializer>();

            _matchInfo = matchInfo;
            Player[] players = matchInfo.GetAllPlayers();
            for (int i = 0; i < players.Length; i++)
            {
                players[i].OnPlayerHitEvent += OnPlayerHit;
            }
        }

        private void OnPlayerHit(Player player, float damage)
        {
            if (!player.IsAlive)
            {
                Dictionary<int, List<Player>> playersPerTeam = _matchInfo.GetPlayersPerTeam();
                int playerTeamID = player.TeamID;

                for (int i = 0; i < playersPerTeam[playerTeamID].Count; i++)
                {
                    if (playersPerTeam[playerTeamID][i].IsAlive)
                        return;
                }

                int aliveTeams = 0;
                for (int i = 0; i < playersPerTeam.Count; i++)
                {
                    for (int j = 0; j < playersPerTeam[i].Count; j++)
                    {
                        if (playersPerTeam[i][j].IsAlive)
                        {
                            aliveTeams++;
                            break;
                        }
                    }
                }

                if (aliveTeams <= 1)
                {
                    OnGameFinished();
                }
            }
        }
        
        private void OnGameFinished()
        {
            int playerCount = _matchInfo.GetAllPlayers().Length;

            GameObject gameOverscreenPrefab = null;
            if (playerCount == 2 || playerCount == 4)
            {
                gameOverscreenPrefab = Resources.Load<GameObject>("UI/GameOverScreen-" + playerCount);
            }
            else
            {
                Debug.LogError("Invalid player count");
                return;
            }

            GameObject instance = Instantiate(gameOverscreenPrefab);
            instance.GetComponent<RectTransform>().SetParent(_UIPopUpCanvas.GetComponent<RectTransform>());
            instance.GetComponent<RectTransform>().localScale = Vector3.one;
            instance.GetComponent<GameOverScreen>().Init(_matchInfo.GetAllPlayers());
            _eventSystem.SetSelectedGameObject(instance.GetComponentInChildren<Button>().gameObject);

            _UIPopUpCanvas.transform.FindChild("Backdrop").GetComponent<Image>().DOFade(0.5f, 1f);
                
			AudioManager.Instance.Play (_gameOverSound, false, false);
            AudioManager.Instance.FadeOut(_gameInitializer.BackgroundMusic);

            MatchInfo.Pause();
        }
    }
}