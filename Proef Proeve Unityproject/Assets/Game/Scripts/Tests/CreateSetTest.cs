﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProefProeve.Set;
using UnityEngine;

namespace ProefProeve
{
    [RequireComponent(typeof(PlayerSetBuilder))]
	public class CreateSetTest : MonoBehaviour
	{
        private void Awake()
        {
            GetComponent<PlayerSetBuilder>().SetCreatedEvent += OnSetCreated;
        }

        private void OnDestroy()
        {
            GetComponent<PlayerSetBuilder>().SetCreatedEvent -= OnSetCreated;
        }

        private void OnSetCreated(int playerID, CardSet set)
        {
            Debug.Log(string.Format("Player {0} created set, is set valid?: {1}", playerID, set.IsValid));
            for (int i = 0; i < set.Cards.Length; i++)
            {
                Debug.Log(set.Cards[i].Properties);
            }
        }
    }
}
