﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProefProeve.player;

namespace ProefProeve
{
    public class MatchInfo : MonoBehaviour
    {
        public delegate void OnGameActionEventHandler();
        public static event OnGameActionEventHandler OnGameStartedEvent;
        public static event OnGameActionEventHandler OnGamePausedEvent;
        public static event OnGameActionEventHandler OnGameResumedEvent;

        public const float TeamColorAlpha = 0.55f;
        public static readonly Color[] TeamColors = new Color[] {
            new Color(1, 0, 0, TeamColorAlpha),
            new Color(0, 0, 1, TeamColorAlpha),
            new Color(0, 1, 0, TeamColorAlpha),
            new Color(1, 0.92f, 0.016f, TeamColorAlpha) };
        public static bool IsPaused { private set; get; }

        private Dictionary<PlayerTarget.PlayerPosition, Player> _playerPositions = new Dictionary<PlayerTarget.PlayerPosition, Player>();
        
        public static void Pause()
        {
            IsPaused = true;

            if (OnGamePausedEvent != null)
                OnGamePausedEvent();
        }

        public static void Resume()
        {
            IsPaused = false;

            if (OnGameResumedEvent != null)
                OnGameResumedEvent();
        }

        public void StartGame()
        {
            IsPaused = false;

            if (OnGameStartedEvent != null)
                OnGameStartedEvent();
        }

        public void SetMatchInfo(Player[] players)
        {
            //if(gameMode == GameModes.FreeForAll)
            //{
            SetFreeForAllPositions(players);
            //}
            //else if(gameMode == GameModes.TeamDeathMatch)
            //{
            // SetTeamDeathMatchPositions(players);
            //}

            // Fallback.
        }

        public Player[] GetAllPlayers()
        {
            Player[] players = new Player[_playerPositions.Count];

            int index = 0;
            foreach (var player in _playerPositions)
            {
                players[index] = player.Value;
                index++;
            }

            return players;
        }

        public Dictionary<int, List<Player>> GetPlayersPerTeam()
        {
            Player[] players = GetAllPlayers();
            Dictionary<int, List<Player>> result = new Dictionary<int, List<Player>>();

            for (int i = 0; i < players.Length; i++)
            {
                if (!result.ContainsKey(players[i].TeamID))
                {
                    result[players[i].TeamID] = new List<Player>();
                }

                result[players[i].TeamID].Add(players[i]);
            }

            return result;
        }

        public Player GetPlayerFromPosition(PlayerTarget.PlayerPosition position)
        {
            if (!_playerPositions.ContainsKey(position))
            {
                return null;
            }

            return _playerPositions[position];
        }

        /// <summary>
        /// Sets player positions to free for all positions.
        /// </summary>
        private void SetFreeForAllPositions(Player[] players)
        {
            int length = players.Length;
            for (int i = 0; i < length; i++)
            {
                PlayerTarget.PlayerPosition playerPosition;
                PlayerTarget.PlayerPosition targetPosition;
                switch (players[i].PlayerID)
                {
                    case 0:
                        targetPosition = PlayerTarget.PlayerPosition.RightTop;
                        playerPosition = PlayerTarget.PlayerPosition.LeftTop;
                        break;
                    case 1:
                        targetPosition = PlayerTarget.PlayerPosition.LeftTop;
                        playerPosition = PlayerTarget.PlayerPosition.RightTop;
                        break;
                    case 2:
                        playerPosition = PlayerTarget.PlayerPosition.LeftBottom;
                        targetPosition = PlayerTarget.PlayerPosition.RightBottom;
                        break;
                    case 3:
                        playerPosition = PlayerTarget.PlayerPosition.RightBottom;
                        targetPosition = PlayerTarget.PlayerPosition.LeftBottom;
                        break;
                    default:
                        playerPosition = PlayerTarget.PlayerPosition.LeftTop;
                        targetPosition = PlayerTarget.PlayerPosition.RightTop;
                        break;
                }

                _playerPositions.Add(playerPosition, players[i]);

                players[i].PlayerTarget.SetMatchInfo(this, targetPosition);
            }
        }

        /// <summary>
        /// Sets players on team deathmatch positions.
        /// </summary>
        /// <param name="players">All players playing.</param>
        private void SetTeamDeathMatchPositions(Player[] players)
        {
            bool firstBluePlaced = false;
            bool firstRedPlaced = false;
            int length = players.Length;
            for (int i = 0; i < length; i++)
            {
                PlayerTarget.PlayerPosition playerPosition = PlayerTarget.PlayerPosition.LeftBottom;
                PlayerTarget.PlayerPosition targetPosition = PlayerTarget.PlayerPosition.LeftBottom;
                if (players[i].TeamID == 0)
                {
                    if (!firstRedPlaced)
                    {
                        targetPosition = PlayerTarget.PlayerPosition.RightTop;
                        playerPosition = PlayerTarget.PlayerPosition.LeftTop;
                        firstRedPlaced = true;
                    }
                    else
                    {
                        targetPosition = PlayerTarget.PlayerPosition.RightBottom;
                        playerPosition = PlayerTarget.PlayerPosition.LeftBottom;
                    }
                }
                else if (players[i].TeamID == 1)
                {
                    if (!firstBluePlaced)
                    {
                        targetPosition = PlayerTarget.PlayerPosition.LeftTop;
                        playerPosition = PlayerTarget.PlayerPosition.RightTop;
                        firstBluePlaced = true;
                    }
                    else
                    {
                        targetPosition = PlayerTarget.PlayerPosition.LeftBottom;
                        playerPosition = PlayerTarget.PlayerPosition.RightBottom;
                    }
                }

                if(playerPosition == targetPosition)
                {
                    Debug.LogError("Found dupplicate position!");
                }

                _playerPositions.Add(playerPosition, players[i]);

                players[i].PlayerTarget.SetMatchInfo(this, targetPosition);
            }
        }
    }

}
