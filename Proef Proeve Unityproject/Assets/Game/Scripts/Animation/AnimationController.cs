﻿using UnityEngine;
using Spine;
using System.Collections.Generic;
using DG.Tweening;

namespace ProefProeve.Animation
{
    [RequireComponent(typeof(Spine.Unity.SkeletonAnimation))]
    public class AnimationController : MonoBehaviour
    {
        /// <summary>
        /// Get the length of the current animation playing.
        /// </summary>
        public float AnimationLength
        {
            get
            {
                if (_skeletonAnimation.state.GetCurrent(0) != null)
                {
                    return _skeletonAnimation.state.GetCurrent(0).Animation.Duration;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Get the name of the current animation playing.
        /// </summary>
        public string AnimationName
        {
            get
            {
                if (_skeletonAnimation.state.GetCurrent(0) != null)
                {
                    return _skeletonAnimation.state.GetCurrent(0).Animation.Name;
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Gets or sets the timescale of this animationcontroller.
        /// </summary>
        public float TimeScale
        {
            get
            {
                return _skeletonAnimation.timeScale;
            }
            set
            {
                _skeletonAnimation.timeScale = value;
            }
        }

        /// <summary>
        /// Gets the length of the animation by name.
        /// </summary>
        /// <param name="name">Name of animation you want the length of.</param>
        /// <returns>Length of specified animation.</returns>
        public float GetAnimationLengthByName(string name)
        {
            return _skeletonAnimation.skeletonDataAsset.GetSkeletonData(false).FindAnimation(name).duration;
        }

        public Spine.Unity.SkeletonAnimation SkeletonAnimation
        {
            get
            {
                return _skeletonAnimation;
            }
        }

        // Delegates
        public delegate void OnAnimationBegin();
        public event OnAnimationBegin OnAnimationBeginHandler;

        public delegate void OnAnimationEnd();
        public event OnAnimationEnd OnAnimationEndHandler;

        public delegate void OnAnimationCompleted();
        public event OnAnimationCompleted OnAnimationCompletedHandler;

        public delegate void OnAnimationEventSendHandler(AnimationController sender, Spine.Event evt);
        public static event OnAnimationEventSendHandler OnAnimationEventSend;

        // Variables
        [SerializeField] private Spine.Unity.SkeletonAnimation _skeletonAnimation;
        [SerializeField] private List<AnimationEntry> _animations;
        [SerializeField] private bool _invokeAnimationEvents = true;

        private AnimationEntry _current;
        private AnimationEntry _nextQueue;
        private AnimationEntry _next;

        private bool _interrupted;
        private bool _hasListeners;

        /// <summary>
        /// Plays the animation entry with specified key.
        /// </summary>
        /// <param name="key"></param>
        public void Play(string key)
        {
            RegisterListeners();

            bool found = false;

            foreach (AnimationEntry animation in _animations)
            {
                if (animation.Key == key)
                {
                    _nextQueue = animation;
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                Debug.LogWarning("[AnimationController] No animation with key: " + key + " exists on " + transform.name + ".", this);
            }
        }

        /// <summary>
        /// Immediately plays the animation entry with specified key.
        /// </summary>
        /// <param name="key"></param>
        public void PlayImmediate(string key)
        {
            _interrupted = true;
            Play(key);
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            RemoveListeners();
        }

        private void LateUpdate()
        {
            if (_nextQueue != null)
            {
                _next = _nextQueue;
                _nextQueue = null;
                _skeletonAnimation.state.SetAnimation(0, _next.Animation, _next.Loop);
            }
        }

        private void Reset()
        {
            _skeletonAnimation = GetComponent<Spine.Unity.SkeletonAnimation>();
        }

        // Events and Handlers.
        private void RegisterListeners()
        {
            if (!_hasListeners && _skeletonAnimation.state != null)
            {
                _skeletonAnimation.state.Start += OnSpineStateStartEvent;
                _skeletonAnimation.state.End += OnSpineStateEndEvent;
                _skeletonAnimation.state.Complete += OnSpineStateCompleteEvent;
                _skeletonAnimation.state.Event += StateEventHandler;

                for (int i = 0; i < _animations.Count; i++)
                {
                    _animations[i].OnAnimationCompleteWithNextEvent += OnAnimationCompleteWithNextEventHandler;
                }

                _hasListeners = true;
            }
        }

        private void RemoveListeners()
        {
            if (_hasListeners && _skeletonAnimation.state != null)
            {
                _skeletonAnimation.state.Start -= OnSpineStateStartEvent;
                _skeletonAnimation.state.End -= OnSpineStateEndEvent;
                _skeletonAnimation.state.Complete -= OnSpineStateCompleteEvent;

                for (int i = 0; i < _animations.Count; i++)
                {
                    _animations[i].OnAnimationCompleteWithNextEvent -= OnAnimationCompleteWithNextEventHandler;
                }

                _hasListeners = false;
            }
        }

        private void OnAnimationCompleteWithNextEventHandler(AnimationEntry next)
        {
            Play(next.Key);
        }

        private void StateEventHandler(TrackEntry entry, Spine.Event e)
        {
            if (_invokeAnimationEvents)
            {
                //Debug.Log("Animation event fired: " + e.Data.Name);

                if(OnAnimationEventSend != null)
                {
                    OnAnimationEventSend(this, e);
                }
            }
        }

        private void OnSpineStateStartEvent(TrackEntry entry)
        {
            _current = _next;
            _current.OnStart(this);

            if (OnAnimationBeginHandler != null)
            {
                OnAnimationBeginHandler();
            }
        }

        private void OnSpineStateEndEvent(TrackEntry entry)
        {
            if (!_interrupted)
            {
                _current.OnEnd(this);
                //_current = null;

                if (OnAnimationEndHandler != null)
                {
                    OnAnimationEndHandler();
                }

            }

            _interrupted = false;
        }

        private void OnSpineStateCompleteEvent(TrackEntry entry)
        {
            _current.OnComplete(this);

            if (OnAnimationCompletedHandler != null)
            {
                OnAnimationCompletedHandler();
            }
        }
    }
}