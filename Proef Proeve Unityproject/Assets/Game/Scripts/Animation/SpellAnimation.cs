﻿using UnityEngine;
using ProefProeve.Animation;
using ProefProeve.Audio;

public class SpellAnimation : MonoBehaviour {
    private const string ANIMATION_NAME = "Fire";

    [SerializeField] private AudioAsset _audioEffect;
    private AnimationController _animationcontroller;

	void Start ()
    {
        _animationcontroller = GetComponent<AnimationController>();

        _animationcontroller.Play(ANIMATION_NAME);

        AudioManager.Instance.Play(_audioEffect, false, true);

        Invoke("DestroyAfterAnimationLength", Time.maximumDeltaTime);
    }

    private void DestroyAfterAnimationLength()
    {
        Invoke("DestroyMe", _animationcontroller.AnimationLength);
    }

    private void DestroyMe()
    {
        Destroy(this.gameObject);
    }
}
