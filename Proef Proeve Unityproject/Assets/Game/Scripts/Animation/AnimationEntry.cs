﻿using System.Collections.Generic;
using UnityEngine;

namespace ProefProeve.Animation
{
    public class AnimationEntry : MonoBehaviour
    {
        public delegate void OnAnimationCompleteWithNextEventHandler(AnimationEntry next);
        public event OnAnimationCompleteWithNextEventHandler OnAnimationCompleteWithNextEvent;

        /// <summary>
        /// Sets or gets the animation name.
        /// </summary>
        public string Animation
        {
            get
            {
                return spineAnimation;
            }
            set
            {
                spineAnimation = value;
            }
        }

        /// <summary>
        /// Gets or sets animation entry key.
        /// </summary>
        public string Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value;
            }
        }

        /// <summary>
        /// Gets if animation loops or not.
        /// </summary>
        public bool Loop
        {
            get
            {
                return _loop;
            }
        }

        [SerializeField][Spine.Unity.SpineAnimation] private string spineAnimation;
        [SerializeField] private string _key;
        [SerializeField] private bool _loop;
        [SerializeField] private AnimationEntry _playOnDone;
        
        [Header("Inspector Util")]
        [SerializeField] private bool setKeyToAnimationName;


        // Event handlers.
        public virtual void OnStart(AnimationController animationController)
        {
            //TODO: onstart logic.
        }

        public virtual void OnEnd(AnimationController animationController)
        {
            //TODO: onend logic.
        }

        public virtual void OnComplete(AnimationController animationController)
        {
            //TODO: oncomplete logic.
            if(_playOnDone != null && OnAnimationCompleteWithNextEvent != null)
            {
                OnAnimationCompleteWithNextEvent(_playOnDone);
            }
        }

        private void Reset()
        {
            _key = spineAnimation;
        }

        private void OnValidate()
        {
            if (setKeyToAnimationName)
            {
                _key = spineAnimation;
                setKeyToAnimationName = false;
            }
        }
    }
}