﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProefProeve.Audio;
using ProefProeve.UI;
using UnityEngine;

namespace ProefProeve
{
    [RequireComponent(typeof(GameRulesPanel))]
	public class InfoPageSwitchSound : MonoBehaviour
	{
        [SerializeField] private AudioAsset _audioAsset;

        private GameRulesPanel _rulesPanel;

        private void Awake()
        {
            _rulesPanel = GetComponent<GameRulesPanel>();
            _rulesPanel.PageSwitchedEvent += OnPageSwitched;
        }

        private void OnDestroy()
        {
            _rulesPanel.PageSwitchedEvent -= OnPageSwitched;
        }

        private void OnPageSwitched()
        {
            AudioManager.Instance.Play(_audioAsset);
        }
    }
}
