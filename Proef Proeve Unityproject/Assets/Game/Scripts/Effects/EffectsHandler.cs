﻿using UnityEngine;
using ProefProeve.player;
using ProefProeve.Powerup;
using ProefProeve.Set;
using ProefProeve.Animation;

namespace ProefProeve
{
    public class EffectsHandler : MonoBehaviour
    {
        private const float ARMOR_Y_OFFSET = 400f;
        private const float CRITICAL_Y_OFFSET = 350f;

        private const string FLASH_SCREEN_PATH = "FlashScreen";
        private const string TEXT_FIELD_PATH = "UI/TextField";

        private enum EventMethods
        {
            Flash,
            Shake
        }

        [System.Serializable]
        private struct ElementAnimation
        {
            public GameObject Animation;
            public CardProperties.Element Element;
            public EventProperty[] EventProperties;
        }

        [System.Serializable]
        private struct EventProperty
        {
            public string EventName;
            public EventMethods EventMethod;
        }

        [SerializeField] private GameObject _criticalHitEffect;
        [SerializeField] private GameObject _healEffect;
        [SerializeField] private GameObject _armorEffect;
        [SerializeField] private ElementAnimation[] _elementAnimations;

        private Player[] _players;
        private GameObject _flashScreen;
        private GameObject _textField;

        public void Initialize(Player[] players)
        {
            _players = players;

            RegisterEventListeners();
        }

        private void RegisterEventListeners()
        {
            int length = _players.Length;
            for (int i = 0; i < length; i++)
            {
                Player player = _players[i];
                player.OnPlayerCriticalHitEvent += OnPlayerCriticalHitEventHandler;
                player.OnPlayerHealedEvent += OnPlayerHealedEventHandler;
                player.OnPlayerHitEvent += OnPlayerHitEventHandler;
                player.OnPlayerPowerupRegisteredEvent += OnPlayerPowerupRegisteredEventHandler;
                player.OnPlayerAttackedEvent += OnPlayerAttackedEventHandler;
            }

            AnimationController.OnAnimationEventSend += OnAnimationEventSendHandler;
        }

        private void RemoveEventListeners()
        {
            int length = _players.Length;
            for (int i = 0; i < length; i++)
            {
                Player player = _players[i];
                player.OnPlayerCriticalHitEvent -= OnPlayerCriticalHitEventHandler;
                player.OnPlayerHealedEvent -= OnPlayerHealedEventHandler;
                player.OnPlayerHitEvent -= OnPlayerHitEventHandler;
            }

            AnimationController.OnAnimationEventSend -= OnAnimationEventSendHandler;
        }

        private void OnDisable()
        {
            RemoveEventListeners();
        }

        private void OnAnimationEventSendHandler(AnimationController sender, Spine.Event evt)
        {
            int length = _elementAnimations.Length;
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < _elementAnimations[i].EventProperties.Length; j++)
                {
                    EventProperty eventProperty = _elementAnimations[i].EventProperties[j];
                    if(eventProperty.EventName == evt.Data.Name)
                    {
                        Invoke(eventProperty.EventMethod.ToString(), 0);
                    }
                }
            }
        }

        private void OnPlayerAttackedEventHandler(Player player, Player target, Set.CardProperties.Element element)
        {
            int length = _elementAnimations.Length;
            for (int i = 0; i < length; i++)
            {
                ElementAnimation elementAnimation = _elementAnimations[i];
                if (elementAnimation.Element == element)
                {
                    // Using heal offset because it is on the feet of the target.
                    CreateEffect(elementAnimation.Animation, target.transform, 0, 0);
                    break;
                }
            }
        }

        private void OnPlayerPowerupRegisteredEventHandler(Player player, PowerupEffect effect)
        {
            if(effect is Armor)
            {
                GameObject newArmorEffect = CreateEffect(_armorEffect, player.transform, 0, ARMOR_Y_OFFSET);
                newArmorEffect.transform.position += new Vector3(0, 0, -1);
                Armor armorEffect = (Armor)effect;
                armorEffect.SetEffect(newArmorEffect);
            }
        }

        private void OnPlayerHitEventHandler(Player player, float damage)
        {
            if(_textField == null)
            {
                _textField = Resources.Load(TEXT_FIELD_PATH, typeof(GameObject)) as GameObject;
            }

            GameObject mainCanvas = GameObject.FindGameObjectWithTag(Tags.MAIN_CANVAS).gameObject;
            GameObject newTextField = Instantiate(_textField);

            RectTransform rectTransform = (RectTransform)newTextField.transform;
            rectTransform.SetParent(mainCanvas.transform);
            rectTransform.localScale = new Vector3(1, 1, 1);

            Vector3 pos = player.transform.position;
            pos.y += Random.Range(6, 10);
            pos.x += Random.Range(-5, 5);
            rectTransform.position = pos;

            newTextField.GetComponent<UnityEngine.UI.Text>().text = string.Format("{0}", damage);
        }

        private void OnPlayerHealedEventHandler(Player player)
        {
            CreateEffect(_healEffect, player.transform, 0, 0);
        }

        private void OnPlayerCriticalHitEventHandler(Player player)
        {
            CreateEffect(_criticalHitEffect, player.transform, 0, CRITICAL_Y_OFFSET);
        }

        private GameObject CreateEffect(GameObject obj, Transform parent, float xOffset, float yOffset)
        {
            GameObject newEffect = Instantiate(obj);
            newEffect.transform.SetParent(parent);
            newEffect.transform.localScale = new Vector3(1, 1, 1);
            newEffect.transform.localPosition = new Vector3(xOffset, yOffset, 0);

            return newEffect;
        }

        private void Flash()
        {
            if(_flashScreen == null)
            {
                _flashScreen = Resources.Load(FLASH_SCREEN_PATH, typeof(GameObject)) as GameObject;
            }

            GameObject mainCanvas = GameObject.FindGameObjectWithTag(Tags.MAIN_CANVAS).gameObject;
            GameObject newFlash = Instantiate(_flashScreen) as GameObject;
            newFlash.transform.SetParent(mainCanvas.transform);

            RectTransform rectTransform = (RectTransform)newFlash.transform;
            rectTransform.localPosition = Vector3.zero;
            rectTransform.localScale = new Vector3(1, 1, 1);

            Destroy(newFlash, 0.1f);
        }

        private void Shake()
        {
            Debug.Log("<color=yellow> Shake not yet implemented, create here!</color>");
        }
    }

}
