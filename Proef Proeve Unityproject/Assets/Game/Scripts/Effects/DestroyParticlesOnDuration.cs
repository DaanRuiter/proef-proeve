﻿using UnityEngine;

namespace ProefProeve
{
    public class DestroyParticlesOnDuration : MonoBehaviour
    {
        private ParticleSystem _particleSystem;

        void Start()
        {
            _particleSystem = GetComponent<ParticleSystem>();
            Invoke("DestroyMe", _particleSystem.main.duration + _particleSystem.main.startLifetime.constant);
        }

        void DestroyMe()
        {
            Destroy(this.gameObject);
        }
    }

}
