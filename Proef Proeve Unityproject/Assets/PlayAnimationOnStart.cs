﻿using UnityEngine;

namespace ProefProeve.Animation
{
    public class PlayAnimationOnStart : MonoBehaviour
    {
        [SerializeField] private AnimationController _animationController;
        [SerializeField] private AnimationEntry _animation;


        void Start()
        {
            _animationController.Play(_animation.Key);
        }
    }

}
